# -*- coding: utf-8 -*-
"""
Created on Wed Sep 17 19:35:20 2014

@author: mill
"""
GRID_SIZE = 800
REAL_SIZE = 1.0
dx = REAL_SIZE/GRID_SIZE
CFL_NUMBER = 0.475
MAX_STEPS = 500


globalWorkSize = (GRID_SIZE)
localWorkSize = (1,1)

import pyopencl as cl
import pyopencl.array as cl_array
import numpy as np
import pylab as py


def CreateProgram(context, fileName, buildOption):
    kernelFile = open(fileName, 'r')
    kernelStr = kernelFile.read()
    program = cl.Program(context, kernelStr)
    program.build(buildOption)
    return program

mf = cl.mem_flags
context = cl.create_some_context()#interactive=True)
queue = cl.CommandQueue(context)
program = CreateProgram(context, "kernel/mhd_kernel.cl", "-I ./kernel/ -Werror")

test_array = np.linspace(2.0, 3.0, num=GRID_SIZE)
#test_array = np.zeros(GRID_SIZE, dtype=cl_array.vec.double8)
test_array = np.linspace(2.0, 3.0, GRID_SIZE)
test_input = cl.Buffer(context, mf.WRITE_ONLY, test_array.nbytes)
test_output = cl.Buffer(context, mf.WRITE_ONLY, test_array.nbytes)
print(test_array)
cl.enqueue_write_buffer(queue, test_input, test_array)
program.foo(queue, test_array.shape, None, test_input, test_output)
cl.enqueue_copy(queue, test_array, test_output)
print(test_array)
print('test done')

#conserv_array = np.zeros(GRID_SIZE, dtype=cl_array.vec.double8)
conserv_array = np.zeros(GRID_SIZE*8, dtype=np.float64)
smax_inverse_array = np.zeros(GRID_SIZE, dtype=np.float64)
left_vectors = [1.0,0.0,0.0,0.0,1.0,1.0,0.0,2.5]
right_vectors = [0.125,0.0,0.0,0.0,1.0,-1.0,0.0,1.15]
conserv_array[0:8] = left_vectors
for x in range(0,GRID_SIZE/2):
    conserv_array[0+8*x:8+8*x] = left_vectors
for x in range(GRID_SIZE/2,GRID_SIZE):
    conserv_array[0+8*x:8+8*x] = right_vectors
print(conserv_array)

##CONSTRUCT MEM IN HOST
interface_array = np.zeros(GRID_SIZE*2, dtype=cl_array.vec.double8)
advance_array = np.zeros(GRID_SIZE*8, dtype=np.float64)

##CONSTRUCT MEM IN DEVICE
conserv_buffer = cl.Buffer(context, mf.READ_WRITE, conserv_array.nbytes)
advance_buffer = cl.Buffer(context, mf.READ_WRITE, conserv_array.nbytes)
interface_buffer = cl.Buffer(context, mf.READ_WRITE, interface_array.nbytes)
debug_buffer = cl.Buffer(context, mf.READ_WRITE, conserv_array.nbytes)
smax_inverse_buffer = cl.Buffer(context, mf.READ_WRITE, smax_inverse_array.nbytes)

##MAIN LOOP
cumm_time = 0.0;
for x in range(MAX_STEPS):
    cl.enqueue_copy(queue, conserv_buffer ,conserv_array)
    program.mhd_smax_inverse(queue,[GRID_SIZE],None, conserv_buffer, smax_inverse_buffer)
    cl.enqueue_copy(queue, smax_inverse_array, smax_inverse_buffer)
    dt = CFL_NUMBER*min(smax_inverse_array[1:-1])*dx
    program.mhd_plm_hll(queue, smax_inverse_array.shape, None, 
                        conserv_buffer,
                        advance_buffer, 
                        smax_inverse_buffer, 
                        np.int32(GRID_SIZE),np.float64(dt),np.float64(dx),
                        interface_buffer,
                        debug_buffer)
    cl.enqueue_copy(queue, advance_array, advance_buffer)
    cl.enqueue_copy(queue, smax_inverse_array, smax_inverse_buffer)
    dt = CFL_NUMBER*min(smax_inverse_array[1:-1])*dx
    conserv_array[8:-8] += dt/dx*advance_array[8:-8]
    cumm_time+=dt
    py.figure(figsize=(10, 8), dpi=120)
    py.plot(np.linspace(0.0,REAL_SIZE,num=GRID_SIZE), conserv_array[::8],color='green', marker='o',
     markerfacecolor='blue', label='rho', markersize=2)    
    py.plot(np.linspace(0.0,REAL_SIZE,num=GRID_SIZE), conserv_array[1:-1:8],'x', label='momentum_x' )
    py.legend()
    print(cumm_time)
    py.show()
