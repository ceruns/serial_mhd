# README #


### What is this repository for? ###

* OpenMP version of mhd code
* Plots and save datas in semi-realtime
* Version 0.1

### How do I get set up? ###

#### Summary of set up
##### Miminum requirement
* g++ 4.8
* OpenMP 2.0
* Boost 1.4
* OpenCL supporting CPU
#### Recommended environment
* g++ 4.9
* OpenMP 4.0
* Boost 1.55
* Intel AVX ISA multicore system
* OpenCL supporting accelerator
#### Dependencies
* Boost 1.54
* OpenCL driver
#### Deployment instructions
* In bash shell
* mkdir build && cd build && cmake .. && make