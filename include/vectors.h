#pragma once

#include <iostream>
#include "eos.h"

class U
{
public:

	double rho;
	double rhovx;
	double rhovy;
	double rhovz;
	double bx;
	double by;
	double bz;
	double Etot;

	U(){};

	U(double rho,double vx,double vy,double vz,double bx,double by,double bz, double p)
	{
		this->rho = rho;
		this->rhovx = rho*vx;
		this->rhovy = rho*vy;
		this->rhovz = rho*vz;

		this->bx = bx;
		this->by = by;
		this->bz = bz;

		this->Etot = p/(get_gamma()-1) + 0.5*( rho*(vx*vx + vy*vy + vz*vz) + (bx*bx + by*by + bz*bz) );

	};
	
//	U(double rho, double vx, double p)
//	{
//		this->rho= rho;
//		this->rhovx = rho*vx;
//		this->Etot = p/get_gamma() + 0.5*(rho*vx*vx);
//	};

	void set(double rho, double rhovx, double rhovy, double rhovz, double bx, double by, double bz, double Etot)
	{
		this->rho = rho;
		this->rhovx = rhovx;
		this->rhovy = rhovy;
		this->rhovz = rhovz;
		this->bx = bx;
		this->by = by;
		this->bz = bz;
		this->Etot = Etot;
	};



	U operator+(const U& u)
	{
		U temp;
		temp.set(
			rho + u.rho,
			rhovx + u.rhovx,
			rhovy + u.rhovy,
			rhovz + u.rhovz,
			bx + u.bx,
			by + u.by,
			bz + u.bz,
			Etot + u.Etot
			);
		return temp;
	};

	U operator-(const U& u)
	{
		U temp;
		temp.set(
			rho - u.rho,
			rhovx - u.rhovx,
			rhovy - u.rhovy,
			rhovz - u.rhovz,
			bx - u.bx,
			by - u.by,
			bz - u.bz,
			Etot - u.Etot
			);
		return temp;
	};

	U operator*(double factor)
	{
		U temp;
		temp.set(
			rho*factor,
			rhovx*factor,
			rhovy*factor,
			rhovz*factor,
			bx*factor,
			by*factor,
			bz*factor,
			Etot*factor
			);
		return temp;
	};

	double get_eint()
	{
		return (this->Etot - 0.5*( (rhovx*rhovx + rhovy*rhovy + rhovz*rhovz)/rho + (bx*bx + by*by + bz*bz) ));
	};

	double get_p()
	{
		return get_eint()*(get_gamma()-1)*rho;
	};

	double get_ptot()
	{
		return get_p()+0.5*(bx*bx + by*by + bz*bz);
	};

	void print()
	{
	std::cout<<" rho: "<<rho<< "\t rhovx: "<<rhovx<< "\t rhovy: "<<rhovy<< "\t rhovz: "<<rhovz<< "\t bx: "<<bx<< "\t by: "<<by<< "\t bz: "<<bz<<"\t Etot: "<<Etot<<std::endl;
	};
	void simple_print()
	{
		std::cout<<rho<< "\t"<<rhovx<< "\t"<<rhovy<< "\t"<<rhovz<< "\t"<<bx<< "\t"<<by<< "\t"<<bz<<"\t"<<Etot<<std::endl;
	};

	double get_alfven_wave_speed()
	{
		return std::abs(bx)/sqrt(rho);
	};

	double get_magneto_wave_speed_alpha()
	{
		double small = get_gamma()*get_p()+(bx*bx + by*by + bz*bz);
		
		double part1 = sqrt(
			(
				small
				+sqrt(
					small*small - 
					4*get_gamma()*get_p()*bx*bx)
			)
			/(2*rho)
		);

		return part1;
	};

	double get_magneto_wave_speed_beta()
	{
		double small = get_gamma()*get_p()+(bx*bx + by*by + bz*bz);
		double part1 = sqrt(
			(
				small
				-sqrt(
					small*small - 
					4*get_gamma()*get_p()*bx*bx)
			)
			/(2*rho)
		);
		return part1;
	};

	std::vector<double> get_data()
	{
		std::vector<double> data;
		data.push_back(rho);
		data.push_back(rhovx);
		data.push_back(rhovy);
		data.push_back(rhovz);
		data.push_back(bx);
		data.push_back(by);
		data.push_back(bz);
		data.push_back(Etot);

		return data;
	};

	void set_data(std::vector<double> data)
	{
		/**
		* @todo : exception handling
		*/
		rho = data[0];
		rhovx = data[1];
		rhovy = data[2];
		rhovz = data[3];
		bx = data[4];
		by = data[5];
		bz = data[6];
		Etot = data[7];

	};

	~U(){};

};

class Flux
{
public:
	double mass; 
	double momentumx; 
	double momentumy; 
	double momentumz; 
	double maxwellx; 
	double maxwelly; 
	double maxwellz; 
	double energy;
	Flux(){};
	Flux(double mass, double momentumx, double momentumy, double momentumz, double maxwellx, double maxwelly, double maxwellz, double energy)
	{
		this->mass = mass; 
		this->momentumx = momentumx; 
		this->momentumy = momentumy; 
		this->momentumz = momentumz; 
		this->maxwellx = maxwellx; 
		this->maxwelly = maxwelly; 
		this->maxwellz = maxwellz; 
		this->energy = energy;
	};

	Flux(U u)
	{
		double rho = u.rho;
		double rhovx = u.rhovx;
		double rhovy = u.rhovy;
		double rhovz = u.rhovz;
		double bx = u.bx;
		double by = u.by;
		double bz = u.bz;
		double Etot = u.Etot;
		double ptot = u.get_ptot();
		this->mass = rhovx;
		this->momentumx = rhovx*rhovx/rho + ptot - bx*bx;
		this->momentumy = rhovx*rhovy/rho - bx*by;
		this->momentumz = rhovx*rhovz/rho - bx*bz;
		this->maxwellx = (bx*rhovx - rhovx*bx)/rho;
		this->maxwelly = (by*rhovx - bx*rhovy)/rho;
		this->maxwellz = (bz*rhovx - bx*rhovz)/rho;
		this->energy = ((Etot + ptot)*rhovx - bx*(bx*rhovx + by*rhovy + bz*rhovz))/rho;
	};
	
	Flux operator+(const Flux& f)
	{
		Flux temp(
			mass+f.mass,
			momentumx+f.momentumx,
			momentumy+f.momentumy,
			momentumz+f.momentumz,
			maxwellx+f.maxwellx,
			maxwelly+f.maxwelly,
			maxwellz+f.maxwellz,
			energy+f.energy
			);
		return temp;
	};
	
	Flux operator-(const Flux& f)
	{
		Flux temp(
			mass-f.mass,
			momentumx-f.momentumx,
			momentumy-f.momentumy,
			momentumz-f.momentumz,
			maxwellx-f.maxwellx,
			maxwelly-f.maxwelly,
			maxwellz-f.maxwellz,
			energy-f.energy
			);
		return temp;
	};

	Flux operator*(double factor)
	{
		Flux temp(
			mass*factor,
			momentumx*factor,
			momentumy*factor,
			momentumz*factor,
			maxwellx*factor,
			maxwelly*factor,
			maxwellz*factor,
			energy*factor
			);
		return temp;
	};

	std::vector<double> get_data()
	{
		std::vector<double> data;
		data.push_back(mass);
		data.push_back(momentumx);
		data.push_back(momentumy);
		data.push_back(momentumz);
		data.push_back(maxwellx);
		data.push_back(maxwelly);
		data.push_back(maxwellz);
		data.push_back(energy);

		return data;
	};

	void set_data(std::vector<double> data)
	{
		/**
		* @todo : exception handling
		*/
		
		mass = data[0];
		momentumx = data[1];
		momentumy = data[2];
		momentumz = data[3];
		maxwellx = data[4];
		maxwelly = data[5];
		maxwellz = data[6];
		energy = data[7];

	};
	
	~Flux(){};
};


/**
class q
{
	
public:
	double rho;
	double u;
	double p;

	q(double rho, double u, double p)
	{
		this->rho = rho;
		this->u = u;
		this->p = p;
	};
	~q(){};


	
};
*/
