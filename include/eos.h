#pragma once

#include "vectors.h"

static const double const_gamma = 5.0/3.0;

inline double get_gamma()
{
	return const_gamma;
};
