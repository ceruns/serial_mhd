#pragma once
#include "vectors.h"

class EulerIntegrator
{
	std::vector<U>* source_grid;
	std::vector<U>* target_grid;
public:
	EulerIntegrator(std::vector<U>* source_grid, std::vector<U>* target_grid)
	{
		this->source_grid = source_grid;
		this->target_grid = target_grid;

	};
	~EulerIntegrator()
	{};

	void set_source_grid(std::vector<U>* grid)
	{
		this->source_grid = grid;
	};

	void set_target_grid(std::vector<U>* grid)
	{
		this->target_grid = grid;
	};

	void set_grid(std::vector<U>* source_grid, std::vector<U>* target_grid)
	{
		this->source_grid = source_grid;
		this->target_grid = target_grid;
	};

	void integrate(int index, double delta_t, double delta_x, Flux left_flux, Flux right_flux)
	{
		Flux flux_diff = right_flux - left_flux;
		flux_diff = flux_diff*delta_t;
		U temp;
		temp.set_data(flux_diff.get_data());
		temp = temp*(1/delta_x);

		temp = source_grid->at(index) - temp;

		target_grid->at(index) = temp;

	};
	
};