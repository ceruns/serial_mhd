#pragma once
#include "vectors.h"
#include "eos.h"
#include <math.h>
#include <algorithm>

class HLL
{
private:
	double lambda1(U u)
	{
		double lambda;
		double vx = u.rhovx/u.rho;
		lambda = vx-u.get_magneto_wave_speed_alpha();
		return lambda;
	};

	double lambda2(U u)
	{
		double lambda;
		double vx = u.rhovx/u.rho;
		lambda = vx-u.get_alfven_wave_speed();
		return lambda;
	};

	double lambda3(U u)
	{
		double lambda;
		double vx = u.rhovx/u.rho;
		lambda = vx-u.get_magneto_wave_speed_beta();
		return lambda;
	};

	double lambda4(U u)
	{
		double lambda;
		double vx = u.rhovx/u.rho;
		lambda = vx;
		return lambda;
	};

	double lambda5(U u)
	{
		double lambda;
		double vx = u.rhovx/u.rho;
		lambda = vx+u.get_magneto_wave_speed_beta();
		return lambda;
	};

	double lambda6(U u)
	{
		double lambda;
		double vx = u.rhovx/u.rho;
		lambda = vx+u.get_alfven_wave_speed();
		return lambda;
	};

	double lambda7(U u)
	{
		double lambda;
		double vx = u.rhovx/u.rho;
		lambda = vx+u.get_magneto_wave_speed_alpha();
		return lambda;
	};

	double lambda_l(U u)
	{
		double lambda_l;

		lambda_l = lambda1(u); 
		lambda_l = std::min(lambda_l, lambda2(u));
		lambda_l = std::min(lambda_l, lambda3(u));
		lambda_l = std::min(lambda_l, lambda4(u));
		lambda_l = std::min(lambda_l, lambda5(u));
		lambda_l = std::min(lambda_l, lambda6(u));
		lambda_l = std::min(lambda_l, lambda7(u));

		return lambda_l;
	};

	double lambda_m(U u)
	{
		double lambda_m;

		lambda_m = lambda1(u);
		lambda_m = std::max(lambda_m, lambda2(u));
		lambda_m = std::max(lambda_m, lambda3(u));
		lambda_m = std::max(lambda_m, lambda4(u));
		lambda_m = std::max(lambda_m, lambda5(u));
		lambda_m = std::max(lambda_m, lambda6(u));
		lambda_m = std::max(lambda_m, lambda7(u));

		return lambda_m;
	};

public:
	HLL(){};
	Flux getF(U left, U right)
	{
		double sl = std::min(lambda_l(left), lambda_l(right));
		double sr = std::max(lambda_m(left), lambda_m(right));


		sl = std::min(sl, 0.0);
		sr = std::max(sr, 0.0);
		
		Flux f_star;

		U temp_U = right - left;
		temp_U = temp_U*(sl*sr);
		f_star.set_data(temp_U.get_data());

		Flux f_l(left);
		Flux f_r(right);

		f_l = f_l*sr;
		f_r = f_r*sl;

		f_star = f_star + f_l;
		f_star = f_star - f_r;
		f_star =  f_star * (1/(sr-sl));



		return f_star;



	};

	~HLL(){};
};