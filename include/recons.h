#pragma once

#include <iostream>
#include <vector>
#include <algorithm>
#include <math.h>

#include "vectors.h"


class PieceConst
{
	std::vector<U> *grid;
public:
	PieceConst(std::vector<U> * grid)
	{
		this->grid = grid;
	};
	~PieceConst()
	{};

	/**@todo : exception handling */

	U get_minus_half_left(int index)
	{
		return grid->at(index-1);
	};
	U get_minus_half_right(int index)
	{
		return grid->at(index);
	};
	U get_plus_half_left(int index)
	{
		return grid->at(index);
	};
	U get_plus_half_right(int index)
	{
		return grid->at(index+1);
	};

	void set_grid(std::vector<U> * grid)
	{

	};
	
};