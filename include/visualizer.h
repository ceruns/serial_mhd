#pragma once
#include <vector>  
#include <cmath>  
#include <boost/tuple/tuple.hpp>
#include <gnuplot-iostream.h>
#include <iostream>
#include <fstream>
#include <string>
#include "vectors.h"

class visualizer
{
	Gnuplot gp;
	Gnuplot gp_save;
	std::vector<double> x;
	std::vector<double> rho;
	std::vector<double> vx;
	std::vector<double> vy;
	std::vector<double> vz;
	std::vector<double> bx;
	std::vector<double> by;
	std::vector<double> bz;
	std::vector<double> Etot;
	double xinit;
	double deltax;
	double deltat;
	double current_time;
	int time_step;

public:
	visualizer(std::vector<U> u, double xinit, double deltax, double deltat, int time_step)
	{
		gp<<"set title "<<"'MHD : Deltax = "<<std::to_string(deltax)<<"'\n";
		gp<<"set ylabel 'data values'\n";
		gp<<"set autoscale\n";

		gp_save<<"set title "<<"'MHD : Deltax = "<<std::to_string(deltax)<<"'\n";
		gp_save<<"set ylabel 'data values'\n";
		gp_save<<"set autoscale\n";
		gp_save<<"set term png size 800,600\n";

		this->time_step = time_step;
		this->xinit = xinit;
		this->deltax = deltax;
		this->deltat = deltat;
		for (uint i = 0; i < u.size(); ++i)
		{
			x.push_back(xinit + (double)i*deltax);
			rho.push_back(u.at(i).rho);
			vx.push_back(u.at(i).rhovx/u.at(i).rho);
			vy.push_back(u.at(i).rhovy/u.at(i).rho);
			vz.push_back(u.at(i).rhovz/u.at(i).rho);
			bx.push_back(u.at(i).bx);
			by.push_back(u.at(i).by);
			bz.push_back(u.at(i).bz);
			Etot.push_back(u.at(i).Etot);
		}
		set_data(u);
		show();
	};
	~visualizer(){};

	void set_data(std::vector<U> u)
	{
		current_time += deltat*(double)time_step; //@to-do fix "staggered time error"
		
		x.clear();
		rho.clear();
		vx.clear();
		vy.clear();
		vz.clear();
		bx.clear();
		by.clear();
		bz.clear();
		Etot.clear();

		for (uint i = 0; i < u.size(); ++i)
		{
			x.push_back(xinit + (double)i*deltax);
			rho.push_back(u.at(i).rho);
			vx.push_back(u.at(i).rhovx/u.at(i).rho);
			vy.push_back(u.at(i).rhovy/u.at(i).rho);
			vz.push_back(u.at(i).rhovz/u.at(i).rho);
			bx.push_back(u.at(i).bx);
			by.push_back(u.at(i).by);
			bz.push_back(u.at(i).bz);
			Etot.push_back(u.at(i).Etot);
		}
	};

	void show()
	{
		//gp << "set xrange [0:" + std::to_string(x.size()*deltax) + "]\n set yrange [0:1]\n";
		gp<<"set xlabel 'current time : "+std::to_string(current_time)+"sec '\n";
		gp << "plot '-' with linespoints title 'rho', "
		   << "'-' with linespoints title 'vx',"
		   << "'-' with linespoints title 'vy',"
		   << "'-' with linespoints title 'vz',"
		   << "'-' with linespoints title 'bx',"
		   << "'-' with linespoints title 'by',"
		   << "'-' with linespoints title 'bz',"
		   << "'-' with linespoints title 'E' \n";
		gp.send1d(boost::make_tuple(x,rho));
		gp.send1d(boost::make_tuple(x,vx));
		gp.send1d(boost::make_tuple(x,vy));
		gp.send1d(boost::make_tuple(x,vz));
		gp.send1d(boost::make_tuple(x,bx));
		gp.send1d(boost::make_tuple(x,by));
		gp.send1d(boost::make_tuple(x,bz));
		gp.send1d(boost::make_tuple(x,Etot));
	};

	void set_data_and_show(std::vector<U> u)
	{
		set_data(u);
		show();
	};

	void save(std::string file_name = "result_")
	{

		gp_save<<"set output \"./"<<file_name<<std::to_string(current_time)<<".png\"\n";

		gp_save<<"set xlabel 'current time : "+std::to_string(current_time)+"sec '\n";
		gp_save << "plot '-' with linespoints title 'rho', "
		   << "'-' with linespoints title 'vx',"
		   << "'-' with linespoints title 'vy',"
		   << "'-' with linespoints title 'vz',"
		   << "'-' with linespoints title 'bx',"
		   << "'-' with linespoints title 'by',"
		   << "'-' with linespoints title 'bz',"
		   << "'-' with linespoints title 'E' \n";
		gp_save.send1d(boost::make_tuple(x,rho));
		gp_save.send1d(boost::make_tuple(x,vx));
		gp_save.send1d(boost::make_tuple(x,vy));
		gp_save.send1d(boost::make_tuple(x,vz));
		gp_save.send1d(boost::make_tuple(x,bx));
		gp_save.send1d(boost::make_tuple(x,by));
		gp_save.send1d(boost::make_tuple(x,bz));
		gp_save.send1d(boost::make_tuple(x,Etot));
		

		// std::cout<<"data at: "<<time<<"sec in recording"<<std::endl;
	
		// std::string file_name = "./result/" + std::to_string((int)(time*100000.0)) + ".txt";
		// std::string gnuplot_source_file_name = "./" + file_name + ".gplt";
	
		// std::ofstream plotFile(gnuplot_source_file_name);
		// plotFile << "set term png" << std::endl << std::endl;
		// plotFile << "set output " << "\"" + std::to_string((int)(time*100000.0)) + ".png\"" << std::endl;
		// plotFile << "plot [][-1.0:1.0] \"" + std::to_string((int)(time*100000.0)) + ".txt\" u 3 w l" << std::endl;
		// std::ofstream SaveFile(file_name);
		// for (uint i = 0; i < u.size(); ++i)
		// {
		// 	SaveFile << i << "\t" << u[i].rho << "\t" << u[i].rhovx/u[i].rho << "\t" << u[i].rhovy/u[i].rho << "\t" << u[i].by << "\t" << u[i].Etot << std::endl;
		// }
		// SaveFile.close();
		// plotFile.close();

	};
	
};
