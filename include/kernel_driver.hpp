#pragma once
#include <utility>
#define __NO_STD_VECTOR // Use cl::vector instead of STL version
#include <CL/cl.hpp>

#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>
#include <iterator>

class KernelDriver
{
public:
    KernelDriver();
    ~KernelDriver();
private:
    void checkErr(cl_int err, const char *name);

    cl_int err;
    cl::vector< cl::Platform > platformList;
    std::string platformVendor;
    cl_context_properties cprops[3] = {CL_CONTEXT_PLATFORM, (cl_context_properties)(platformList[0])(), 0 };
    cl::Context context;
 
    double U_vectors[ARRAY_SIZE *NUM_OF_EQ];
    double advance_vectors[ARRAY_SIZE *NUM_OF_EQ];
    double smax_inverse[ARRAY_SIZE];
    int grid_size = (int)ARRAY_SIZE;

    cl::Buffer U_buffer;
    cl::Buffer advance_buffer;
    cl::Buffer smax_buffer;

    cl::vector<cl::Device> devices;
    devices = context.getInfo<CL_CONTEXT_DEVICES>();

    cl::CommandQueue mhd_queue;
    cl::Event mhd_event;

    double cumm_time;
};