//#pragma OPENCL EXTENSION cl_khr_fp64 : enable
#define GROUP_SIZE 128
__kernel void mhd_plm_hll(__global double* grid, __global double* grid_advance, __global double* smax_inverse, int grid_size);


__kernel void mhd_plm_hll(__global double* grid, __global double* grid_advance, __global double* smax_inverse, int grid_size)
{
    size_t gid = get_global_id(0); // i-th global thread computes i-th U_advance
    if(gid>= grid_size)
    {
        return;
    }

    size_t lid = get_local_id(0);
    size_t local_position = lid +2;
    __local double8 local_grid[LOCAL_SIZE + 4];
    //Load U vectors into local memeory
    local_grid[local_position] = (grid[gid*8 + 0],grid[gid*8 + 1],grid[gid*8 + 2],grid[gid*8 + 3],grid[gid*8 + 4],grid[gid*8 + 5],grid[gid*8 + 6],grid[gid*8 + 7]);
    if(gid<2 && lid==0) // Left boundary
    {
        local_grid[local_position-2] = local_grid[local_position];
        local_grid[local_position-1] = local_grid[local_position];
    }
    else if(lid==0)
    {
        local_grid[local_position-2] = (grid[(gid-2)*8+ 0], grid[(gid-2)*8+ 1], grid[(gid-2)*8+ 2], grid[(gid-2)*8+ 3], grid[(gid-2)*8+ 4], grid[(gid-2)*8+ 5], grid[(gid-2)*8+ 6], grid[(gid-2)*8+ 7]);
        local_grid[local_position-1] = (grid[(gid-1)*8+ 0], grid[(gid-1)*8+ 1], grid[(gid-1)*8+ 2], grid[(gid-1)*8+ 3], grid[(gid-1)*8+ 4], grid[(gid-1)*8+ 5], grid[(gid-1)*8+ 6], grid[(gid-1)*8+ 7]);
    }
    
    if(gid>grid_size-3 && lid==LOCAL_SIZE-1) // Right boundary
    {
        local_grid[local_position +1] = local_grid[local_position];
        local_grid[local_position +2] = local_grid[local_position];
    }
    else if(lid==LOCAL_SIZE-1)
    {
        local_grid[local_position +1] = (grid[(gid+1)*8 + 0],grid[(gid+1)*8 + 1],grid[(gid+1)*8 + 2],grid[(gid+1)*8 + 3],grid[(gid+1)*8 + 4],grid[(gid+1)*8 + 5],grid[(gid+1)*8 + 6],grid[(gid+1)*8 + 7]);
        local_grid[local_position +2] = (grid[(gid+2)*8 + 0],grid[(gid+2)*8 + 1],grid[(gid+2)*8 + 2],grid[(gid+2)*8 + 3],grid[(gid+2)*8 + 4],grid[(gid+2)*8 + 5],grid[(gid+2)*8 + 6],grid[(gid+2)*8 + 7]);
    }

    barrier(CLK_LOCAL_MEM_FENCE);

    __private double8 flux[2];

    grid_advance[gid*8 + 0] = local_grid[local_position].S0;
    grid_advance[gid*8 + 1] = local_grid[local_position].S1;
    grid_advance[gid*8 + 2] = local_grid[local_position].S2;
    grid_advance[gid*8 + 3] = local_grid[local_position].S3;
    grid_advance[gid*8 + 4] = local_grid[local_position].S4;
    grid_advance[gid*8 + 5] = local_grid[local_position].S5;
    grid_advance[gid*8 + 6] = local_grid[local_position].S6;
    grid_advance[gid*8 + 7] = local_grid[local_position].S7;
    barrier(CLK_GLOBAL_MEM_FENCE);
    return;
}