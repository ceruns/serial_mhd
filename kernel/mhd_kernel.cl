//#pragma OPENCL EXTENSION cl_khr_fp64 : enable // Now it is core functionality of opencl not an extention, but left for OpenCl 1.1 devices
#include "reconstruction.h"
#include "solver.h"
#include "vector_util.h"

__kernel void foo(__global double *input, __global double * output)
{
    size_t gid = get_global_id(0);
    output[gid] = 2.0*input[gid];
    return;
}


__kernel void mhd_plm_hll(__global double *grid, __global double *grid_advance, __global double *smax_inverse, int grid_size, double dt, double dx, __global double8 *interface_grid, __global double *debugging_grid);
__kernel void mhd_pc_hll( __global double *grid, __global double *grid_advance, __global double *smax_inverse, int grid_size); // Kernel

__kernel void mhd_smax_inverse(__global double *grid, __global double *smax_inverse)
{
    size_t tid = get_global_id(0);

    double8 conserv = (double8) (
                                  grid[tid * 8 + 0],
                                  grid[tid * 8 + 1],
                                  grid[tid * 8 + 2],
                                  grid[tid * 8 + 3],
                                  grid[tid * 8 + 4],
                                  grid[tid * 8 + 5],
                                  grid[tid * 8 + 6],
                                  grid[tid * 8 + 7]
                              );
    double8 q_vect = u_to_q(conserv);
    double smax_inverse_temp = get_max_double8(get_eigenvalues_q(q_vect));
    smax_inverse[tid] = 1.0 / smax_inverse_temp;
    barrier(CLK_GLOBAL_MEM_FENCE);

    return;

}

/*
// Gets advance U vector
// Input : cells, U^(n) vectors
// Output : cell U^(n+1) vectors
// Output : inverse of s_max to compute min del_t
*/
__kernel void mhd_pc_hll( __global double *grid, __global double *grid_advance, __global double *smax_inverse, int grid_size)
{
    int tid = get_global_id(0); // i-th thread computes i-th U_advance, i.e. tid means cell number
    if (tid >= grid_size - 1 || tid <= 1)
    {
        return;
    }
    private double8 prim[3]; //Where input data will be loaded, one double8 data means one U vector. For Piecewise recons and HLL, 0:left cell 1: center cell 2: right cell
    private double8 flux[2]; //Two fluxes are computed on one cell

    // Data allocation via Piecewise Constant
    recons_pc(grid, prim, grid_size, tid);
    // prim[] set.

    //Get fluxes
    hll_solver(prim, flux, smax_inverse, tid);
    //grid_advance is only For debugging

    private double8 advance_U = (double8)(flux[0] - flux[1]);

    //Generate Output. Note that smax_inverse should be generated in hll_solver()
    grid_advance[tid * 8 + 0] = advance_U.S0;
    grid_advance[tid * 8 + 1] = advance_U.S1;
    grid_advance[tid * 8 + 2] = advance_U.S2;
    grid_advance[tid * 8 + 3] = advance_U.S3;
    grid_advance[tid * 8 + 4] = advance_U.S4;
    grid_advance[tid * 8 + 5] = advance_U.S5;
    grid_advance[tid * 8 + 6] = advance_U.S6;
    grid_advance[tid * 8 + 7] = advance_U.S7;
    return;
}

__kernel void mhd_plm_hll
(
    __global double *grid,
    __global double *grid_advance,
    __global double *smax_inverse,
    int grid_size,
    double dt,
    double dx,
    __global double8 *interface_grid,
    __global double *debugging_grid
)
{

    const int tid = get_global_id(0); // i-th thread computes i-th U_advance, i.e. tid means cell number

    private double8 prim[3]; //Where input data will be loaded, one double8 data means one U vector. For Piecewise recons and HLL, 0:left cell 1: center cell 2: right cell

    // Data allocation via Piecewise Linear Method
    private double8 inner_interface[2];

    recons_pl(grid, prim, grid_size, tid, dt, dx, inner_interface, debugging_grid);
    // prim[] set.
    barrier(CLK_GLOBAL_MEM_FENCE);
    interface_grid[tid * 2    ] = inner_interface[0]; // -1/2 R
    barrier(CLK_GLOBAL_MEM_FENCE);
    interface_grid[tid * 2 + 1] = inner_interface[1]; // +1/2 L
    private double8 outer_interface[2];
    barrier(CLK_GLOBAL_MEM_FENCE);
    outer_interface[0] = interface_grid[tid * 2 - 1]; // -1/2 L
    outer_interface[1] = interface_grid[tid * 2 + 2]; // +1/2 R
    inner_interface[0] = interface_grid[tid * 2    ];
    inner_interface[1] = interface_grid[tid * 2 + 1];
    barrier(CLK_GLOBAL_MEM_FENCE);
    private double8 flux[2]; //Two fluxes are computed on one cell
    //Get fluxes
    general_hlle_solver(prim, flux, inner_interface, outer_interface, smax_inverse, tid, debugging_grid);
    //Generate Output. Note that smax_inverse should be generated in hll_solver()
    private double8 advance_U = (double8)(flux[0] - flux[1]);

    grid_advance[tid * 8 + 0] = advance_U.S0;
    grid_advance[tid * 8 + 1] = advance_U.S1;
    grid_advance[tid * 8 + 2] = advance_U.S2;
    grid_advance[tid * 8 + 3] = advance_U.S3;
    grid_advance[tid * 8 + 4] = advance_U.S4;
    grid_advance[tid * 8 + 5] = advance_U.S5;
    grid_advance[tid * 8 + 6] = advance_U.S6;
    grid_advance[tid * 8 + 7] = advance_U.S7;

    return;
}
