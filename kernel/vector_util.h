#pragma once
#include "eos.h"

inline double8 u_to_q(double8 u_vector); //@TESTED Convert one double8 vector(U) to q (or W)
inline double8 q_to_u(double8 u_vector); //@TESTED Reverse function of u_to_q
inline double8 get_eigenvalues_q(double8 q_vector); //@TESTED Get the set of eigenvalues from q vector
void get_eigenvectorLR(double8 q_vector, double8 *left_ev , double8 *right_ev);  // Get the matrix of left and right vectors
inline double get_max_double8(double8 vect); //@TESTED
inline double get_min_double8(double8 vect); //@TESTED
void characteristic_tracing(double8 *stencil, double8 *left_eigen_mat, double8 *right_eigen_mat, double8 target_vector, double dt, double dx);
inline double get_vect_elem_at(double8 vect, int i)
{
    if (i == 0) return vect.S0;
    else if (i == 1) return vect.S1;
    else if (i == 2) return vect.S2;
    else if (i == 3) return vect.S3;
    else if (i == 4) return vect.S4;
    else if (i == 5) return vect.S5;
    else if (i == 6) return vect.S6;
    else return vect.S7;
}
inline void write_double8_to_debugging_grid(__global double *debugging_grid, double8 target_vect, size_t pos)
{
    debugging_grid[pos * 8 + 0] = target_vect.S0;
    debugging_grid[pos * 8 + 1] = target_vect.S1;
    debugging_grid[pos * 8 + 2] = target_vect.S2;
    debugging_grid[pos * 8 + 3] = target_vect.S3;
    debugging_grid[pos * 8 + 4] = target_vect.S4;
    debugging_grid[pos * 8 + 5] = target_vect.S5;
    debugging_grid[pos * 8 + 6] = target_vect.S6;
    debugging_grid[pos * 8 + 7] = target_vect.S7;
}

inline double get_max_double8(double8 vect) // Returns max value from double8 (considering only 7 values)
{
    double lambda_max;
    lambda_max = vect.S0;
    lambda_max = fmax(lambda_max, vect.S1);
    lambda_max = fmax(lambda_max, vect.S2);
    lambda_max = fmax(lambda_max, vect.S3);
    lambda_max = fmax(lambda_max, vect.S4);
    lambda_max = fmax(lambda_max, vect.S5);
    lambda_max = fmax(lambda_max, vect.S6);
    lambda_max = fmax(lambda_max, vect.S7);
    return lambda_max;
}
inline double get_min_double8(double8 vect) // Returns min value from double8 (considering only 7 values)
{
    double lambda_min;
    lambda_min = vect.S0;
    lambda_min = fmin(lambda_min, vect.S1);
    lambda_min = fmin(lambda_min, vect.S2);
    lambda_min = fmin(lambda_min, vect.S3);
    lambda_min = fmin(lambda_min, vect.S4);
    lambda_min = fmin(lambda_min, vect.S5);
    lambda_min = fmin(lambda_min, vect.S6);
    lambda_min = fmin(lambda_min, vect.S7);
    return lambda_min;
}

inline double8 u_to_q(double8 u_vector)
{
    double rho = u_vector.s0;
    double u = u_vector.s1 / u_vector.s0;
    double v = u_vector.s2 / u_vector.s0;
    double w = u_vector.s3 / u_vector.s0;
    double v2 = u * u + v * v + w * w;
    double bx = u_vector.s4;
    double by = u_vector.s5;
    double bz = u_vector.s6;
    double b2 = bx * bx + by * by + bz * bz;
    double Energy_tot = u_vector.s7;
    double pressure = (gamma - 1) * (Energy_tot - 0.5 * rho * v2 - 0.5 * b2);
    return (double8)(
               u_vector.S0,
               u_vector.S1 / u_vector.S0,
               u_vector.S2 / u_vector.S0,
               u_vector.S3 / u_vector.S0,
               u_vector.S4,
               u_vector.S5,
               u_vector.S6,
               pressure
           );
}
inline double8 q_to_u(double8 q_vector)
{
    double rho = q_vector.s0;
    double u = q_vector.s1;
    double v = q_vector.s2;
    double w = q_vector.s3;
    double v2 = u * u + v * v + w * w;
    double bx = q_vector.s4;
    double by = q_vector.s5;
    double bz = q_vector.s6;
    double b2 = bx * bx + by * by + bz * bz;
    double pressure = q_vector.s7;
    double Energy_tot = pressure / (gamma - 1.0) + 0.5 * (rho * v2 + b2);
    return (double8)(
               q_vector.S0,
               q_vector.S1 * q_vector.S0,
               q_vector.S2 * q_vector.S0,
               q_vector.S3 * q_vector.S0,
               q_vector.S4,
               q_vector.S5,
               q_vector.S6,
               Energy_tot
           );
}

inline double8 get_eigenvalues_q(double8 q_vector)
{
    double pressure = q_vector.s7;
    double rho = q_vector.s0;
    double a_square = gamma * pressure / rho;
    double v = q_vector.s1;
    double bx = q_vector.s4;
    double by = q_vector.s5;
    double bz = q_vector.s6;
    double b2 = bx * bx + by * by + bz * bz;
    double alfven_speed_square = b2 / rho;
    double alfven_speed = sqrt(alfven_speed_square);
    double alfven_speed_x_square = bx * bx / rho;
    double alfven_speed_x = sqrt(alfven_speed_x_square);
    double fast_magneto_sonic = sqrt(
                                    0.5 *
                                    (
                                        (a_square + alfven_speed_square) +
                                        sqrt(
                                            (a_square + alfven_speed_square) * (a_square + alfven_speed_square) -
                                            4.0 * a_square * alfven_speed_x_square
                                        )
                                    )
                                );
    double slow_magneto_sonic = sqrt(
                                    0.5 *
                                    (
                                        (a_square + alfven_speed_square) -
                                        sqrt(
                                            (a_square + alfven_speed_square) * (a_square + alfven_speed_square) -
                                            4.0 * a_square * alfven_speed_x_square
                                        )
                                    )
                                );
    return (double8)
           (
               v - fast_magneto_sonic,
               v - alfven_speed_x,
               v - slow_magneto_sonic,
               v,
               v,
               v + slow_magneto_sonic,
               v + alfven_speed_x,
               v + fast_magneto_sonic
           );
}

void get_eigenvectorLR(double8 q_vector,  double8 *left_ev ,  double8 *right_ev)
{
    __private double8 eigenvalues = get_eigenvalues_q(q_vector);

    double Cs = eigenvalues.S5 - q_vector.S1;
    double Ca = eigenvalues.S6 - q_vector.S1;
    double Cf = eigenvalues.S7 - q_vector.S1;

    double acoustic_speed = sqrt(gamma * q_vector.S7 / q_vector.S0);             // regular acoustic wavespeed

    double  alpha_f = 1.0;
    double  alpha_s = 0.0;

    if (Cf != Cs)
    {
        alpha_f = sqrt( (acoustic_speed * acoustic_speed - Cs * Cs) / (Cf * Cf - Cs * Cs) );
        alpha_s = sqrt( (Cf * Cf - acoustic_speed * acoustic_speed) / (Cf * Cf - Cs * Cs) );
    }
    double beta_y = q_vector.S5 / sqrt(q_vector.S5 * q_vector.S5 + q_vector.S6 * q_vector.S6);
    double beta_z = q_vector.S6 / sqrt(q_vector.S5 * q_vector.S5 + q_vector.S6 * q_vector.S6);

    double sgnBx = (double)sign(q_vector.S4);

    const size_t minus_Cf    = 0;
    const size_t minus_Ca    = 1;
    const size_t minus_Cs    = 2;
    const size_t entropy     = 3;
    const size_t divergence  = 4;
    const size_t plus_Cs     = 5;
    const size_t plus_Ca     = 6;
    const size_t plus_Cf     = 7;


    left_ev[minus_Cf] = (double8)
                        (
                            0.0,
                            -1.0 * (alpha_f * Cf) / (2.0 * acoustic_speed * acoustic_speed),
                            (alpha_s / (2.0 * acoustic_speed * acoustic_speed)) * Cs * beta_y * sgnBx, // problem
                            (alpha_s / (2.0 * acoustic_speed * acoustic_speed)) * Cs * beta_z * sgnBx, // problem
                            0.0,
                            (alpha_s / (2.0 * sqrt(q_vector.S0) * acoustic_speed)) * beta_y,  // problem
                            (alpha_s / (2.0 * sqrt(q_vector.S0) * acoustic_speed)) * beta_z,  // problem
                            alpha_f / (2.0 * q_vector.S0 * acoustic_speed * acoustic_speed)
                        );


    left_ev[plus_Cf] = (double8)
                       (
                           left_ev[minus_Cf].S0,
                           -left_ev[minus_Cf].S1,
                           -left_ev[minus_Cf].S2,
                           -left_ev[minus_Cf].S3,
                           left_ev[minus_Cf].S4,
                           left_ev[minus_Cf].S5,
                           left_ev[minus_Cf].S6,
                           left_ev[minus_Cf].S7
                       );
    left_ev[minus_Ca] = (double8)
                        (
                            0.0,
                            0.0,
                            -1.0 * beta_z / sqrt(2.0),
                            beta_y / sqrt(2.0),
                            0.0,
                            -1.0 * beta_z / sqrt(2.0 * q_vector.S0),
                            beta_y / sqrt(2.0 * q_vector.S0),
                            0.0
                        );
    left_ev[plus_Ca] = (double8)
                       (
                           left_ev[minus_Ca].S0,
                           left_ev[minus_Ca].S1,
                           left_ev[minus_Ca].S2,
                           left_ev[minus_Ca].S3,
                           left_ev[minus_Ca].S4,
                           -left_ev[minus_Ca].S5,
                           -left_ev[minus_Ca].S6,
                           left_ev[minus_Ca].S7
                       );
    left_ev[minus_Cs] = (double8)
                        (
                            0.0,
                            -1.0 * (alpha_s * Cs) / (2.0 * acoustic_speed * acoustic_speed),
                            -1.0 * (alpha_f / (2.0 * acoustic_speed * acoustic_speed)) * Cf * beta_y * sgnBx,
                            -1.0 * (alpha_f / (2.0 * acoustic_speed * acoustic_speed)) * Cf * beta_z * sgnBx,
                            0.0,
                            -1.0 * (alpha_f / (2.0 * sqrt(q_vector.S0) * acoustic_speed)) * beta_y,
                            -1.0 * (alpha_f / (2.0 * sqrt(q_vector.S0) * acoustic_speed)) * beta_z,
                            alpha_s / (2.0 * q_vector.S0 * acoustic_speed * acoustic_speed)
                        );
    left_ev[plus_Cs] = (double8)
                       (
                           left_ev[minus_Cs].S0,
                           -left_ev[minus_Cs].S1,
                           -left_ev[minus_Cs].S2,
                           -left_ev[minus_Cs].S3,
                           left_ev[minus_Cs].S4,
                           left_ev[minus_Cs].S5,
                           left_ev[minus_Cs].S6,
                           left_ev[minus_Cs].S7
                       );
    left_ev[divergence] = (double8)
                          (
                              0.0,
                              0.0,
                              0.0,
                              0.0,
                              1.0,
                              0.0,
                              0.0,
                              0.0
                          );
    left_ev[entropy] = (double8)
                       (
                           1.0,
                           0.0,
                           0.0,
                           0.0,
                           0.0,
                           0.0,
                           0.0,
                           -1.0 / (acoustic_speed * acoustic_speed)
                       );


    right_ev[minus_Cf] = (double8)
                         (
                             q_vector.S0 * alpha_f,
                             -1.0 * alpha_f * Cf,
                             alpha_s * Cs * beta_y * sgnBx,
                             alpha_s * Cs * beta_z * sgnBx,
                             0.0,
                             alpha_s * sqrt(q_vector.S0) * acoustic_speed * beta_y,
                             alpha_s * sqrt(q_vector.S0) * acoustic_speed * beta_z,
                             alpha_f * gamma * q_vector.S7
                         );
    right_ev[plus_Cf] = (double8)
                        (
                            right_ev[minus_Cf].S0,
                            -right_ev[minus_Cf].S1,
                            -right_ev[minus_Cf].S2,
                            -right_ev[minus_Cf].S3,
                            right_ev[minus_Cf].S4,
                            right_ev[minus_Cf].S5,
                            right_ev[minus_Cf].S6,
                            right_ev[minus_Cf].S7
                        );
    right_ev[minus_Ca] = (double8)
                         (
                             left_ev[minus_Ca].S0,
                             left_ev[minus_Ca].S1,
                             left_ev[minus_Ca].S2,
                             left_ev[minus_Ca].S3,
                             left_ev[minus_Ca].S4,
                             -1.0 * sqrt(q_vector.S0 / 2.0) * beta_z,
                             sqrt(q_vector.S0 / 2.0) * beta_y,
                             left_ev[minus_Ca].S7
                         );
    right_ev[plus_Ca] = (double8)
                        (
                            right_ev[minus_Ca].S0,
                            right_ev[minus_Ca].S1,
                            right_ev[minus_Ca].S2,
                            right_ev[minus_Ca].S3,
                            right_ev[minus_Ca].S4,
                            -right_ev[minus_Ca].S5,
                            -right_ev[minus_Ca].S6,
                            right_ev[minus_Ca].S7
                        );
    right_ev[minus_Cs] = (double8)
                         (
                             q_vector.S0 * alpha_s,
                             -1.0 * alpha_s * Cs,
                             -1.0 * alpha_f * Cf * beta_y * sgnBx,
                             -1.0 * alpha_f * Cf * beta_z * sgnBx,
                             0.0,
                             -1.0 * alpha_f * sqrt(q_vector.S0) * acoustic_speed * beta_y,
                             -1.0 * alpha_f * sqrt(q_vector.S0) * acoustic_speed * beta_z,
                             alpha_s * gamma * q_vector.S7
                         );
    right_ev[plus_Cs] = (double8)
                        (
                            right_ev[minus_Cs].S0,
                            -right_ev[minus_Cs].S1,
                            -right_ev[minus_Cs].S2,
                            -right_ev[minus_Cs].S3,
                            right_ev[minus_Cs].S4,
                            right_ev[minus_Cs].S5,
                            right_ev[minus_Cs].S6,
                            right_ev[minus_Cs].S7
                        );
    right_ev[divergence] = left_ev[divergence];
    right_ev[entropy] = (double8)
                        (
                            1.0,
                            0.0,
                            0.0,
                            0.0,
                            0.0,
                            0.0,
                            0.0,
                            0.0
                        );

}

inline double8 minmod_limiter(double8 center, double8 left, double8 right)
{
    double8 return_vect;
    left = fabs(left) * 2.0;
    right = fabs(right) * 2.0;
    return_vect = fabs(center);
    return_vect = fmin(return_vect, left);
    return_vect = fmin(return_vect, right);
    return_vect = sign(center)*return_vect;
    return return_vect;
}

void characteristic_tracing
(
    double8 *stencil, // continuing stencil for 3 double8 vectors
    double8 *left_eigen_mat, // left eigen matrix with 8 double8 vectors
    double8 *right_eigen_mat, // right eigen matrix with 8 double8 vectors
    double8 target_vector,// target vector to be chracteristc traced
    double dt,
    double dx
)
{
    __private double8 eigenvalues_center_cell = get_eigenvalues_q(stencil[1]);
    double eigenvalue_center_max = get_max_double8(eigenvalues_center_cell);
    double eigenvalue_center_min = get_min_double8(eigenvalues_center_cell);
    double8 minus_right_multiplier = (double8)(eigenvalue_center_min) - eigenvalues_center_cell;
    double8 plus_left_multiplier = eigenvalues_center_cell - (double8)(eigenvalue_center_max);
#pragma unroll
    for (int i = 0; i < 8; i++)
    {
        __private double traversing_eigen_value = get_vect_elem_at(eigenvalues_center_cell, i);
        if (traversing_eigen_value < 0.0)
        {
            stencil[0] += 0.5 * (dt / dx) *
                          (eigenvalue_center_min - traversing_eigen_value) *
                          (dot(left_eigen_mat[i].s0123, target_vector.s0123) + dot(left_eigen_mat[i].s4567, target_vector.s4567))
                          * right_eigen_mat[i];
        }
        else if (traversing_eigen_value > 0.0)
        {
            stencil[2] += 0.5 * (dt / dx) *
                          (eigenvalue_center_max - traversing_eigen_value) *
                          (dot(left_eigen_mat[i].s0123, target_vector.s0123) + dot(left_eigen_mat[i].s4567, target_vector.s4567))
                          * right_eigen_mat[i];
        }
    }

    // for hll correction
#ifndef NOT_HLL
#pragma unroll
    for (int i = 0; i < 8; i++)
    {
        __private double traversing_eigen_value = get_vect_elem_at(eigenvalues_center_cell, i);
        if (traversing_eigen_value < 0.0)
        {
            stencil[0] += -0.5 * (dt / dx) *
                          (traversing_eigen_value - eigenvalue_center_min) *
                          (dot(left_eigen_mat[i].s0123, target_vector.s0123) + dot(left_eigen_mat[i].s4567, target_vector.s4567))
                          * right_eigen_mat[i];
        }
        else if (traversing_eigen_value > 0.0)
        {
            stencil[2] += -0.5 * (dt / dx) *
                          (traversing_eigen_value - eigenvalue_center_max) *
                          (dot(left_eigen_mat[i].s0123, target_vector.s0123) + dot(left_eigen_mat[i].s4567, target_vector.s4567))
                          * right_eigen_mat[i];
        }
    }
#endif
}

inline double dot_double8(double8 left, double8 right)
{
    double8 result = left * right;
    return result.s0 + result.s1 + result.s2 + result.s3 + result.s4 + result.s5 + result.s6 + result.s7;
}