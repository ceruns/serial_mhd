#pragma once
#include "vector_util.h"
#define nvgi non_vector_grid_id
#define MINMOD_THETA (1.0)
inline size_t non_vector_grid_id(size_t vector_grid_id, size_t component);
void recons_pc(__global double *const grid, __private double8 *prim, size_t grid_size, size_t cell_num); // Piecewise reconstruction
//void recons_pl(__global double *const grid, __private double8 *prim, size_t grid_size, size_t cell_num, double dt, double dx, double8 *interface);  // Piecewise reconstruction
inline double8 minmod(double8 a, double8 b, double8 c);
inline double minmod_point(double a, double b, double c);


inline size_t non_vector_grid_id(size_t vector_grid_id, size_t component)
{
    return vector_grid_id * 8 + component;
}
inline double minmod_point(double a, double b, double c)
{
    double minabs = fabs(a);
    minabs = fmin(minabs, fabs(b));
    minabs = fmin(minabs, fabs(c));
    return 0.25 * (sign(a) + sign(b)) * fabs((sign(a) + sign(c))) * minabs;
}

inline double8 minmod(double8 a, double8 b, double8 c)
{
    return (double8)(minmod_point(a.S0, b.S0, c.S0),
                     minmod_point(a.S1, b.S1, c.S1),
                     minmod_point(a.S2, b.S2, c.S2),
                     minmod_point(a.S3, b.S3, c.S3),
                     minmod_point(a.S4, b.S4, c.S4),
                     minmod_point(a.S5, b.S5, c.S5),
                     minmod_point(a.S6, b.S6, c.S6),
                     minmod_point(a.S7, b.S7, c.S7));
}
void recons_pc(
    __global double *const grid,    // Input grid
    __private double8 *prim,        // Output cell U vectors
    size_t grid_size,                    // Size of total grid
    size_t cell_num                   // num of given cell
)
{
    //Left cell
    prim[0] = (double8)
              (
                  grid[(cell_num - 1) * 8 + 0],
                  grid[(cell_num - 1) * 8 + 1],
                  grid[(cell_num - 1) * 8 + 2],
                  grid[(cell_num - 1) * 8 + 3],
                  grid[(cell_num - 1) * 8 + 4],
                  grid[(cell_num - 1) * 8 + 5],
                  grid[(cell_num - 1) * 8 + 6],
                  grid[(cell_num - 1) * 8 + 7]
              );
    //Center cell
    prim[1] = (double8)
              (
                  grid[cell_num * 8 + 0],
                  grid[cell_num * 8 + 1],
                  grid[cell_num * 8 + 2],
                  grid[cell_num * 8 + 3],
                  grid[cell_num * 8 + 4],
                  grid[cell_num * 8 + 5],
                  grid[cell_num * 8 + 6],
                  grid[cell_num * 8 + 7]
              );
    //Right cell
    prim[2] = (double8)
              (
                  grid[(cell_num + 1) * 8 + 0],
                  grid[(cell_num + 1) * 8 + 1],
                  grid[(cell_num + 1) * 8 + 2],
                  grid[(cell_num + 1) * 8 + 3],
                  grid[(cell_num + 1) * 8 + 4],
                  grid[(cell_num + 1) * 8 + 5],
                  grid[(cell_num + 1) * 8 + 6],
                  grid[(cell_num + 1) * 8 + 7]
              );
}

void recons_pl(
    __global double *const grid,    // Input grid
    __private double8 *prim,        // Output cell U vectors
    size_t grid_size,                    // Size of total grid
    size_t cell_num,                   // num of given cell
    double dt,                      // global dt
    double dx ,                      // global dx
    double8 *interface,
    __global double *debugging_grid
)
{
    // prim is return value.
    // prim[0] : right interface of -1/2
    // prim[1] : ceter cell
    // prim[2] : left interface of +1/2

    // Load given variables into private memory, prim*, making double8
    recons_pc(grid, prim, grid_size, cell_num);

    __private double8 prim_q[3]; // Primitive variables in the form of "q"

    prim_q[0] = u_to_q(prim[0]);
    prim_q[1] = u_to_q(prim[1]);
    prim_q[2] = u_to_q(prim[2]);

    __private double8 prim_diff[3]; // left, centered and right differences of the primitive variables
    prim_diff[0] = prim_q[1] - prim_q[0];
    prim_diff[1] = 0.5 * (prim_q[2] - prim_q[0]);
    prim_diff[2] = prim_q[2] - prim_q[1];
    // Get matrices of eigenvectors
    __private double8 eigenL[8];
    __private double8 eigenR[8];
    get_eigenvectorLR(prim_q[1], eigenL, eigenR);
    // Project into "L"eft matrix of egienvectors.
    __private double8 prim_left_proj_diff[3];
#pragma unroll
    for (size_t i = 0; i < 3; i++)
    {
        prim_left_proj_diff[i].s0 = dot_double8(eigenL[0], prim_diff[i]);
        prim_left_proj_diff[i].s1 = dot_double8(eigenL[1], prim_diff[i]);
        prim_left_proj_diff[i].s2 = dot_double8(eigenL[2], prim_diff[i]);
        prim_left_proj_diff[i].s3 = dot_double8(eigenL[3], prim_diff[i]);
        prim_left_proj_diff[i].s4 = dot_double8(eigenL[4], prim_diff[i]);
        prim_left_proj_diff[i].s5 = dot_double8(eigenL[5], prim_diff[i]);
        prim_left_proj_diff[i].s6 = dot_double8(eigenL[6], prim_diff[i]);
        prim_left_proj_diff[i].s7 = dot_double8(eigenL[7], prim_diff[i]);

    }

    __private double8 mono_diff = minmod_limiter(prim_left_proj_diff[1], prim_left_proj_diff[0], prim_left_proj_diff[2]);
    // Project back to "R"ight matrix of eigenvectors.
    __private double8 mono_prim = (double8) (
                                      dot_double8(eigenR[0], mono_diff),
                                      dot_double8(eigenR[1], mono_diff),
                                      dot_double8(eigenR[2], mono_diff),
                                      dot_double8(eigenR[3], mono_diff),
                                      dot_double8(eigenR[4], mono_diff),
                                      dot_double8(eigenR[5], mono_diff),
                                      dot_double8(eigenR[6], mono_diff),
                                      dot_double8(eigenR[7], mono_diff)
                                  );

    // Compute q ref(prim[0] is R of -1/2, prim[2] is L of +1/2 cell)

    __private double lambda_multiplier_max = fmax(get_max_double8(get_eigenvalues_q(prim_q[1])), 0.0);
    __private double lambda_multiplier_min = fmin(get_min_double8(get_eigenvalues_q(prim_q[1])), 0.0);

    prim_q[0] = prim_q[1] - (0.5 - lambda_multiplier_min * dt / (dx) * 0.5) * mono_prim;
    prim_q[2] = prim_q[1] + (0.5 - lambda_multiplier_max * dt / (dx) * 0.5) * mono_prim;

    //Characteristic tracing
    __private double8 character_tracing_base = (double8) (
                dot_double8(eigenL[0], mono_prim),
                dot_double8(eigenL[1], mono_prim),
                dot_double8(eigenL[2], mono_prim),
                dot_double8(eigenL[3], mono_prim),
                dot_double8(eigenL[4], mono_prim),
                dot_double8(eigenL[5], mono_prim),
                dot_double8(eigenL[6], mono_prim),
                dot_double8(eigenL[7], mono_prim)
            );

    characteristic_tracing(prim_q, eigenL, eigenR, mono_diff, dt, dx);

    interface[0] = q_to_u(prim_q[0]);
    interface[1] = q_to_u(prim_q[2]);
    
    return;


    //@NOTE: HOW TO WRITE TO DEBUGGING BUFFFER
    //write_double8_to_debugging_grid(debugging_grid, prim_diff[0], cell_num);
}



