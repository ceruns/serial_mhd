#include "eos.h"
#include "vector_util.h"
double8 get_eigenvector(__private double8 U, __private double pressure);
double8 get_derived_flux_from_u(__private double8 U, __private double total_pressure); // Return flux which is derived from U vector
double get_total_pressure(__private double8 U, __private double pressure);
double get_pressure(__private double8 U);
void hll_solver(__private double8 *prim, __private double8 *flux, __global double *smax_inverse, int cell_num); // Get two fluxes for one U vector
double get_max_double7(__private double8 vect); // Returns max value from double8 (considering only 7 values)
double get_min_double7(__private double8 vect); // Returns min value from double8 (considering only 7 values)

double8 get_eigenvector(__private double8 U, __private double pressure) // returns eigen vector
{
    double alfven_wave = fabs(U.S4) / sqrt(U.S0);

    double gamma_pressure_bfield = gamma * pressure + U.S4 * U.S4 + U.S5 * U.S5 + U.S6 * U.S6;
    double velocity_x = U.S1 / U.S0;

    double magneto_alpha = sqrt(
                               (
                                   gamma_pressure_bfield +
                                   sqrt(
                                       gamma_pressure_bfield * gamma_pressure_bfield -
                                       4.0 * gamma * pressure * U.S4 * U.S4
                                   )
                               )
                               / (2 * U.S0)
                           );


    double magneto_beta = sqrt(
                              (
                                  gamma_pressure_bfield -
                                  sqrt(
                                      gamma_pressure_bfield * gamma_pressure_bfield -
                                      4.0 * gamma * pressure * U.S4 * U.S4
                                  )
                              )
                              / (2 * U.S0)
                          );

    double8 lambda = (double8)(0.0);
    lambda.S0 = velocity_x - magneto_alpha;
    lambda.S1 = velocity_x - alfven_wave;
    lambda.S2 = velocity_x - magneto_beta;
    lambda.S3 = velocity_x;
    lambda.S4 = velocity_x + magneto_beta;
    lambda.S5 = velocity_x + alfven_wave;
    lambda.S6 = velocity_x + magneto_alpha;
    return lambda;
    //lambda.S7 = 0.0; // Unused value
}

double8 get_derived_flux_from_u(__private double8 U, __private double total_pressure) // Return flux which is derived from U vector
{
    double8 temp_flux = (double8)(0.0);
    temp_flux.S0 = U.S1;
    temp_flux.S1 = U.S1 * U.S1 / U.S0 + total_pressure - U.S4 * U.S4;
    temp_flux.S2 = U.S1 * U.S2 / U.S0 - U.S4 * U.S5;
    temp_flux.S3 = U.S2 * U.S3 / U.S0 - U.S4 * U.S6;
    temp_flux.S4 = U.S4 * U.S1 / U.S0 - U.S4 * U.S1 / U.S0;
    temp_flux.S5 = U.S5 * U.S1 / U.S0 - U.S4 * U.S2 / U.S0;
    temp_flux.S6 = U.S6 * U.S1 / U.S0 - U.S4 * U.S3 / U.S0;
    temp_flux.S7 = (U.S7 + total_pressure) * U.S1 / U.S0 - U.S4 * (U.S4 * U.S1 + U.S5 * U.S2 + U.S6 * U.S3) / U.S0;
    return temp_flux;
}
double get_total_pressure(__private double8 U, __private double pressure)
{
    double p_total = pressure + 0.5 * (U.S4 * U.S4 + U.S5 * U.S5 + U.S6 * U.S6);
    return p_total;
}
double get_pressure(__private double8 U)
{
    double e_int = U.S7 - 0.5 * ((U.S1 * U.S1 + U.S2 * U.S2 + U.S3 * U.S3) / U.S0 + (U.S4 * U.S4 + U.S5 * U.S5 + U.S6 * U.S6));
    double pressure = e_int * (gamma - 1.0);
    return pressure;
}
double get_max_double7(__private double8 vect) // Returns max value from double8 (considering only 7 values)
{
    double lambda_max;
    lambda_max = vect.S0;
    lambda_max = fmax(lambda_max, vect.S1);
    lambda_max = fmax(lambda_max, vect.S2);
    lambda_max = fmax(lambda_max, vect.S3);
    lambda_max = fmax(lambda_max, vect.S4);
    lambda_max = fmax(lambda_max, vect.S5);
    lambda_max = fmax(lambda_max, vect.S6);
    return lambda_max;
}
double get_min_double7(__private double8 vect) // Returns min value from double8 (considering only 7 values)
{
    double lambda_min;
    lambda_min = vect.S0;
    lambda_min = fmin(lambda_min, vect.S1);
    lambda_min = fmin(lambda_min, vect.S2);
    lambda_min = fmin(lambda_min, vect.S3);
    lambda_min = fmin(lambda_min, vect.S4);
    lambda_min = fmin(lambda_min, vect.S5);
    lambda_min = fmin(lambda_min, vect.S6);
    return lambda_min;
}
void hll_solver( // Get two fluxes for one U vector
    __private double8 *prim, // Input U cells
    __private double8 *flux,   // Output fluxes
    __global double *smax_inverse,
    int cell_num)
//)
//Debugging purpose
//, __global double* debugging_grid)
{
    // Index 0 means left, 1 means centre, 2 means right
    double pressure[3];
    double total_pressure[3];
    double8 derived_flux[3];
    double8 eigenvector[3];
    double lambda_max[3];
    double lambda_min[3];
    double sr_left, sr_right, sl_left, sl_right;

    pressure[0] = get_pressure(prim[0]);    // Left Pressure
    pressure[1] = get_pressure(prim[1]);    // Centre Pressure
    pressure[2] = get_pressure(prim[2]);    // Right Pressure

    total_pressure[0] = get_total_pressure(prim[0], pressure[0]); // Left total Pressure
    total_pressure[1] = get_total_pressure(prim[1], pressure[1]); // Centre total Pressure
    total_pressure[2] = get_total_pressure(prim[2], pressure[2]); // Right total Pressure

    derived_flux[0] = get_derived_flux_from_u(prim[0], total_pressure[0]); // Left derived(temporary) fluxes
    derived_flux[1] = get_derived_flux_from_u(prim[1], total_pressure[1]); // Centre derived(temporary) fluxes
    derived_flux[2] = get_derived_flux_from_u(prim[2], total_pressure[2]); // Right derived(temporary) fluxes

    eigenvector[0] = get_eigenvector(prim[0], pressure[0]); // Left eigenvectors.
    eigenvector[1] = get_eigenvector(prim[1], pressure[1]); // Centre eigenvectors.
    eigenvector[2] = get_eigenvector(prim[2], pressure[2]); // Right eigenvectors.

    lambda_max[0] = get_max_double7(eigenvector[0]); // Gets max and min lamdas for each cell
    lambda_max[1] = get_max_double7(eigenvector[1]);
    lambda_max[2] = get_max_double7(eigenvector[2]);

    lambda_min[0] = get_min_double7(eigenvector[0]);
    lambda_min[1] = get_min_double7(eigenvector[1]);
    lambda_min[2] = get_min_double7(eigenvector[2]); //

    sl_left = fmin(lambda_min[0], lambda_min[1]); // Left(half) cell's sl
    sr_left = fmax(lambda_max[0], lambda_max[1]); // Left(half) cell's sr
    sl_right = fmin(lambda_min[1], lambda_min[2]);// Right(half) cell's sl
    sr_right = fmax(lambda_max[1], lambda_max[2]); // Right(half) cell's sr

    smax_inverse[cell_num] = 1.0 / (fmax(fabs(sr_right), fabs(sl_right)));

    sl_left = fmin(0.0, sl_left);
    sr_left = fmax(0.0, sr_left);
    sl_right = fmin(0.0, sl_right);
    sr_right = fmax(0.0, sr_right);

    flux[0] = (double8)((sr_left * derived_flux[0] - sl_left * derived_flux[1] + (sl_left * sr_left) * (prim[1] - prim[0])) * (1 / (sr_left - sl_left))); // Left flux using double 8 calc
    flux[1] = (double8)((sr_right * derived_flux[1] - sl_right * derived_flux[2] + sl_right * sr_right * (prim[2] - prim[1])) * (1 / (sr_right - sl_right))); // Right flux using double 8 calc
}

inline double8 direct_flux_derive(double8 u_vect)
{
    double pressure = get_pressure(u_vect);
    double total_pressure = get_total_pressure(u_vect, pressure);
    return get_derived_flux_from_u(u_vect, total_pressure);
}
void general_hll_solver( // Get two fluxes for one U vector
    __private double8 *prim, // Input U cells
    __private double8 *flux,   // Output fluxes
    __private double8 *inner_interface,
    __private double8 *outer_interface,
    __global double *smax_inverse,
    int cell_num
)
//)
//Debugging purpose
//, __global double* debugging_grid)
{
    // Index 0 means -1/2 left, 1 means -1/2 right, 2 means +1/2 left, 3 means +1/2 right
    double pressure[4];
    double total_pressure[4];
    double8 derived_flux[4];
    double8 eigenvector[4];
    double lambda_max[4];
    double lambda_min[4];
    double sr_left, sr_right, sl_left, sl_right;

    pressure[0] = get_pressure(outer_interface[0]);    //
    pressure[1] = get_pressure(inner_interface[0]);    //
    pressure[2] = get_pressure(inner_interface[1]);    //
    pressure[3] = get_pressure(outer_interface[1]);    //

    total_pressure[0] = get_total_pressure(outer_interface[0], pressure[0]); //
    total_pressure[1] = get_total_pressure(inner_interface[0], pressure[1]); //
    total_pressure[2] = get_total_pressure(inner_interface[1], pressure[2]); //
    total_pressure[3] = get_total_pressure(outer_interface[1], pressure[3]); //

    derived_flux[0] = get_derived_flux_from_u(outer_interface[0], total_pressure[0]); //
    derived_flux[1] = get_derived_flux_from_u(inner_interface[0], total_pressure[1]); //
    derived_flux[2] = get_derived_flux_from_u(inner_interface[1], total_pressure[2]); //
    derived_flux[3] = get_derived_flux_from_u(outer_interface[1], total_pressure[3]); //

    eigenvector[0] = get_eigenvector(outer_interface[0], pressure[0]); //
    eigenvector[1] = get_eigenvector(inner_interface[0], pressure[1]); //
    eigenvector[2] = get_eigenvector(inner_interface[1], pressure[2]); //
    eigenvector[3] = get_eigenvector(outer_interface[1], pressure[3]); //

    lambda_max[0] = get_max_double7(eigenvector[0]); // Gets max and min lamdas for each cell
    lambda_max[1] = get_max_double7(eigenvector[1]);
    lambda_max[2] = get_max_double7(eigenvector[2]);
    lambda_max[3] = get_max_double7(eigenvector[3]);

    lambda_min[0] = get_min_double7(eigenvector[0]);
    lambda_min[1] = get_min_double7(eigenvector[1]);
    lambda_min[2] = get_min_double7(eigenvector[2]); //
    lambda_min[3] = get_min_double7(eigenvector[3]); //

    sl_left = fmin(lambda_min[0], lambda_min[1]); // Left(half) cell's sl
    sr_left = fmax(lambda_max[0], lambda_max[1]); // Left(half) cell's sr
    sl_right = fmin(lambda_min[2], lambda_min[3]);// Right(half) cell's sl
    sr_right = fmax(lambda_max[2], lambda_max[3]); // Right(half) cell's sr

    smax_inverse[cell_num] = 1.0 / (fmax(fabs(sr_right), fabs(sl_right)));

    sl_left = fmin(0.0, sl_left);
    sr_left = fmax(0.0, sr_left);
    sl_right = fmin(0.0, sl_right);
    sr_right = fmax(0.0, sr_right);

    flux[0] = (double8)((sr_left * derived_flux[0] - sl_left * derived_flux[1] + (sl_left * sr_left) * (prim[1] - prim[0])) * (1 / (sr_left - sl_left))); // Left flux using double 8 calc
    flux[1] = (double8)((sr_right * derived_flux[2] - sl_right * derived_flux[3] + (sl_right * sr_right) * (prim[2] - prim[1])) * (1 / (sr_right - sl_right))); // Right flux using double 8 calc
}


void general_hlle_solver( // Get two fluxes for one U vector
    __private double8 *prim, // Input U cells
    __private double8 *flux,   // Output fluxes
    __private double8 *inner_interface,
    __private double8 *outer_interface,
    __global double *smax_inverse,
    int cell_num,
    __global double *debugging_grid
)
{
    double pressure[4];
    double total_pressure[4];
    double8 derived_flux[4];
    double8 eigenvector[4];
    double lambda_max[4];
    double lambda_min[4];
    double sr_right, sl_right;

    pressure[0] = get_pressure(outer_interface[0]);    //
    pressure[1] = get_pressure(inner_interface[0]);    //
    pressure[2] = get_pressure(inner_interface[1]);    //
    pressure[3] = get_pressure(outer_interface[1]);    //

    total_pressure[0] = get_total_pressure(outer_interface[0], pressure[0]); //
    total_pressure[1] = get_total_pressure(inner_interface[0], pressure[1]); //
    total_pressure[2] = get_total_pressure(inner_interface[1], pressure[2]); //
    total_pressure[3] = get_total_pressure(outer_interface[1], pressure[3]); //

    derived_flux[0] = get_derived_flux_from_u(outer_interface[0], total_pressure[0]); //
    derived_flux[1] = get_derived_flux_from_u(inner_interface[0], total_pressure[1]); //
    derived_flux[2] = get_derived_flux_from_u(inner_interface[1], total_pressure[2]); //
    derived_flux[3] = get_derived_flux_from_u(outer_interface[1], total_pressure[3]); //

    eigenvector[0] = get_eigenvector(outer_interface[0], pressure[0]); //
    eigenvector[1] = get_eigenvector(inner_interface[0], pressure[1]); //
    eigenvector[2] = get_eigenvector(inner_interface[1], pressure[2]); //
    eigenvector[3] = get_eigenvector(outer_interface[1], pressure[3]); //

    lambda_max[0] = get_max_double7(eigenvector[0]); // Gets max and min lamdas for each cell
    lambda_max[1] = get_max_double7(eigenvector[1]);
    lambda_max[2] = get_max_double7(eigenvector[2]);
    lambda_max[3] = get_max_double7(eigenvector[3]);

    lambda_min[0] = get_min_double7(eigenvector[0]);
    lambda_min[1] = get_min_double7(eigenvector[1]);
    lambda_min[2] = get_min_double7(eigenvector[2]); //
    lambda_min[3] = get_min_double7(eigenvector[3]); //


    sl_right = fmin(lambda_min[2], lambda_min[3]);// Right(half) cell's sl
    sr_right = fmax(lambda_max[2], lambda_max[3]); // Right(half) cell's sr
    smax_inverse[cell_num] = 1.0 / (fmax(fabs(sr_right), fabs(sl_right)));

    double b_plus_left = fmax(fmax(get_max_double7(get_eigenvector(prim[1], get_pressure(prim[1]))), get_eigenvector(inner_interface[0], pressure[1]).S6), 0.0);
    double b_minus_left = fmin(fmin(get_max_double7(get_eigenvector(prim[1], get_pressure(prim[1]))), get_eigenvector(outer_interface[0], pressure[0]).S0), 0.0);
    double b_plus_right = fmax(fmax(get_max_double7(get_eigenvector(prim[1], get_pressure(prim[1]))), get_eigenvector(inner_interface[1], pressure[3]).S6), 0.0);
    double b_minus_right = fmin(fmin(get_max_double7(get_eigenvector(prim[1], get_pressure(prim[1]))), get_eigenvector(outer_interface[1], pressure[2]).S0), 0.0);

    flux[0] = (b_plus_left * derived_flux[0] - b_minus_left * derived_flux[1]) / (b_plus_left - b_minus_left) + (b_plus_left * b_minus_left / (b_plus_left - b_minus_left)) * (prim[1] - prim[0]);
    flux[1] = (b_plus_right * derived_flux[2] - b_minus_right * derived_flux[3]) / (b_plus_right - b_minus_right) + (b_plus_right * b_minus_right / (b_plus_right - b_minus_right)) * (prim[2] - prim[1]);
    write_double8_to_debugging_grid(debugging_grid, outer_interface[1], cell_num);
}
