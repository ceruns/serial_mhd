void derived_primitive_value(__global double* const grid, __private double8* prim_q, int cell_num); // U -> q
void get_eigenvectors(__private double8 q, __private double8* left_ev, __private double8* right_ev);

double8 get_eigenvalues(__private double8 q);
double get_max_double8(__private double8 vect);
double get_min_double8(__private double8 vect);
double dot_product(__private double8 vect1, __private double8 vect2);
double sgn(__private double value);

double sgn(__private double value);
{
    int result;

    if(value > 0.0)
        result = 1.0;
    else if(value < 0.0)
        result = -1.0;
    else
        result = 0.0;

    return result;
}



double dot_product(__private double8 vect1, __private double8 vect2)
{
    double result;
    result = vect1.S0 * vect2.S0;
    result += vect1.S1 * vect2.S1;
    result += vect1.S2 * vect2.S2;
    result += vect1.S3 * vect2.S3;
    result += vect1.S4 * vect2.S4;
    result += vect1.S5 * vect2.S5;
    result += vect1.S6 * vect2.S6;
    result += vect1.S7 * vect2.S7;
    return result;
}

double get_max_double8(__private double8 vect) // Returns max value from double8
{
    double lambda_max;
    lambda_max = vect.S0;
    lambda_max = fmax(lambda_max, vect.S1);
    lambda_max = fmax(lambda_max, vect.S2);
    lambda_max = fmax(lambda_max, vect.S3);
    lambda_max = fmax(lambda_max, vect.S4);
    lambda_max = fmax(lambda_max, vect.S5);
    lambda_max = fmax(lambda_max, vect.S6);
    lambda_max = fmax(lambda_max, vect.S7);
    return lambda_max;
}
double get_min_double8(__private double8 vect) // Returns min value from double8
{
    double lambda_min;
    lambda_min = vect.S0;
    lambda_min = fmin(lambda_min, vect.S1);
    lambda_min = fmin(lambda_min, vect.S2);
    lambda_min = fmin(lambda_min, vect.S3);
    lambda_min = fmin(lambda_min, vect.S4);
    lambda_min = fmin(lambda_min, vect.S5);
    lambda_min = fmin(lambda_min, vect.S6);
    lambda_min = fmin(lambda_min, vect.S7);
    return lambda_min;
}

double get_smax_inverse(__private double8 q)
{
    double fast_magneto_acoustic = sqrt (
                (
                    gamma_pressure_bfield
                    + sqrt(gamma_pressure_bfield * gamma_pressure_bfield - 4*gamma*q.S7*q.S4*q.S4)
                )/2*q.S0
            );
    return 1/fast_magneto_acoustic;
}

void derived_primitive_value(
    __global double* const grid,    // Input grid
    __private double8* prim_q,      // Output cell q vectors
    int cell_num                    // num of given cell
    )
{
    prim_q[0] = (double8)
          (
           grid[(cell_num-1)*8+0],
           grid[(cell_num-1)*8+1]/grid[(cell_num-1)*8+0],
           grid[(cell_num-1)*8+2]/grid[(cell_num-1)*8+0],
           grid[(cell_num-1)*8+3]/grid[(cell_num-1)*8+0],
           grid[(cell_num-1)*8+4],
           grid[(cell_num-1)*8+5],
           grid[(cell_num-1)*8+6],
           (gamma - 1) * (grid[(cell_num-1)*8+7]
                            -0.5 * ((grid[(cell_num-1)*8+1] * grid[(cell_num-1)*8+1] // rho v^2
                                    +grid[(cell_num-1)*8+2] * grid[(cell_num-1)*8+2]
                                    +grid[(cell_num-1)*8+3] * grid[(cell_num-1)*8+3]
                                    )/grid[(cell_num-1)*8+0]
                                   + grid[(cell_num-1)*8+4] * grid[(cell_num-1)*8+4] // B^2
                                    +grid[(cell_num-1)*8+5] * grid[(cell_num-1)*8+5]
                                    +grid[(cell_num-1)*8+6] * grid[(cell_num-1)*8+6]
                                   )
                         )
          );

    prim_q[1] = (double8)
          (
           grid[(cell_num)*8+0],
           grid[(cell_num)*8+1]/grid[(cell_num)*8+0],
           grid[(cell_num)*8+2]/grid[(cell_num)*8+0],
           grid[(cell_num)*8+3]/grid[(cell_num)*8+0],
           grid[(cell_num)*8+4],
           grid[(cell_num)*8+5],
           grid[(cell_num)*8+6],
           (gamma - 1) * (grid[(cell_num)*8+7]
                            -0.5 * ((grid[(cell_num)*8+1] * grid[(cell_num)*8+1] // rho v^2
                                    +grid[(cell_num)*8+2] * grid[(cell_num)*8+2]
                                    +grid[(cell_num)*8+3] * grid[(cell_num)*8+3]
                                    )/grid[(cell_num)*8+0]
                                   + grid[(cell_num)*8+4] * grid[(cell_num)*8+4] // B^2
                                    +grid[(cell_num)*8+5] * grid[(cell_num)*8+5]
                                    +grid[(cell_num)*8+6] * grid[(cell_num)*8+6]
                                   )
                         )
          );

    prim_q[2] = (double8)
          (
           grid[(cell_num+1)*8+0],
           grid[(cell_num+1)*8+1]/grid[(cell_num+1)*8+0],
           grid[(cell_num+1)*8+2]/grid[(cell_num+1)*8+0],
           grid[(cell_num+1)*8+3]/grid[(cell_num+1)*8+0],
           grid[(cell_num+1)*8+4],
           grid[(cell_num+1)*8+5],
           grid[(cell_num+1)*8+6],
           (gamma - 1) * (grid[(cell_num+1)*8+7]
                            -0.5 * ((grid[(cell_num+1)*8+1] * grid[(cell_num+1)*8+1] // rho v^2
                                    +grid[(cell_num+1)*8+2] * grid[(cell_num+1)*8+2]
                                    +grid[(cell_num+1)*8+3] * grid[(cell_num+1)*8+3]
                                    )/grid[(cell_num+1)*8+0]
                                   + grid[(cell_num+1)*8+4] * grid[(cell_num+1)*8+4] // B^2
                                    +grid[(cell_num+1)*8+5] * grid[(cell_num+1)*8+5]
                                    +grid[(cell_num+1)*8+6] * grid[(cell_num+1)*8+6]
                                   )
                         )
          );

}

double8 get_eigenvalues(__private double8 q)
{
    double alfven_wave = fabs(q.S4)/sqrt(q.S0);

    double gamma_pressure_bfield = gamma * q.S7 + (q.S4*q.S4 + q.S5*q.S5 + q.S6*q.S6)
    double fast_magneto_acoustic = sqrt (
                (
                    gamma_pressure_bfield
                    + sqrt(gamma_pressure_bfield * gamma_pressure_bfield - 4*gamma*q.S7*q.S4*q.S4)
                )/2*q.S0
            )
    double slow_magneto_acoustic = sqrt (
                (
                    gamma_pressure_bfield
                    - sqrt(gamma_pressure_bfield * gamma_pressure_bfield - 4*gamma*q.S7*q.S4*q.S4)
                )/2*q.S0
            )

    double8 lambda = (double8)(0.0);
    lambda.S0 = q.S1 - fast_magneto_acoustic;
    lambda.S1 = q.S1 - alfven_wave;
    lambda.S2 = q.S1 - slow_magneto_acoustic;
    lambda.S3 = q.S1;   // Entropy wave
    lambda.S4 = q.S1;   // Divergence wave
    lambda.S5 = q.S1 + slow_magneto_acoustic;
    lambda.S6 = q.S1 + alfven_wave;
    lambda.S7 = q.S1 + fast_magneto_acoustic;
    return lambda;
}

void get_eigenvectors(
    __private double8 q,        // Input primitive values
    __private double8* left_ev,  //Output left eigenvector
    __private double8* right_ev, //Output right eigenvector
    __private double8 eigenvalues
    )
{
    double Cs = eigenvalues.S5 - q.S1;
    double Ca = eigenvalues.S6 - q.S1;
    double Cf = eigenvalues.S7 - q.S1;

    double a = gamma * q.S7 / q.S0;             // regular acoustic wavespeed

    double alpha_f = sqrt( (a*a - Cs*Cs) / (Cf*Cf - Cs*Cs) );
    double alpha_s = sqrt( (Cf*Cf - a*a) / (Cf*Cf - Cs*Cs) );

    double beta_y = q.S5 / sqrt(q.S5*q.S5 + q.S6*q.S6);
    double beta_z = q.S6 / sqrt(q.S5*q.S5 + q.S6*q.S6);

    double sgnBx = sgn(q.S4);

    const int minus_Cf    = 0;
    const int minus_Ca    = 1;
    const int minus_Cs    = 2;
    const int entropy     = 3;
    const int divergence  = 4;
    const int plus_Cs     = 5;
    const int plus_Ca     = 6;
    const int plus_Cf     = 7;

    left_ev[minus_Cf] = (double8)
        (
         0.0,
         -1.0*(alpha_f * Cf)/(2.0*a*a),
         (alpha_s/(2.0*a*a)) * Cs*beta_y*sgnBx,
         (alpha_s/(2.0*a*a)) * Cs*beta_z*sgnBx,
         0.0,
         (alpha_s/(2.0*sqrt(q.S0)*a))*beta_y,
         (alpha_s/(2.0*sqrt(q.S0)*a))*beta_z,
         alpha_f/(2.0*q.S0*a*a)
        );
    left_ev[plus_Cf] = (double8)
        (
         left_ev[minus_Cf].S0,
         -left_ev[minus_Cf].S1,
         -left_ev[minus_Cf].S2,
         -left_ev[minus_Cf].S3,
         left_ev[minus_Cf].S4,
         left_ev[minus_Cf].S5,
         left_ev[minus_Cf].S6,
         left_ev[minus_Cf].S7
        );
    left_ev[minus_Ca] = (double8)
        (
         0.0,
         0.0,
         -1.0*beta_z/sqrt(2.0),
         beta_y/sqrt(2.0),
         0.0,
         -1.0*beta_z/sqrt(2.0*q.S0),
         beta_y/sqrt(2.0*q.S0),
         0.0
        );
    left_ev[plus_Ca] = (double8)
        (
         left_ev[minus_Ca].S0,
         left_ev[minus_Ca].S1,
         left_ev[minus_Ca].S2,
         left_ev[minus_Ca].S3,
         left_ev[minus_Ca].S4,
         -left_ev[minus_Ca].S5,
         -left_ev[minus_Ca].S6,
         left_ev[minus_Ca].S7,
        );
    left_ev[minus_Cs] = (double8)
        (
         0.0,
         -1.0*(alpha_s * Cs)/(2.0*a*a),
         (alpha_f/(2.0*a*a)) * Cf*beta_y*sgnBx,
         (alpha_f/(2.0*a*a)) * Cf*beta_z*sgnBx,
         0.0,
         -1.0*(alpha_f/(2.0*sqrt(q.S0)*a))*beta_y,
         -1.0*(alpha_f/(2.0*sqrt(q.S0)*a))*beta_z,
         alpha_s/(2.0*q.S0*a*a)
        );
    left_ev[plus_Cs] = (double8)
        (
         left_ev[minus_Cs].S0,
         -left_ev[minus_Cs].S1,
         -left_ev[minus_Cs].S2,
         -left_ev[minus_Cs].S3,
         left_ev[minus_Cs].S4,
         left_ev[minus_Cs].S5,
         left_ev[minus_Cs].S6,
         left_ev[minus_Cs].S7
        );
    left_ev[divergence] = (double8)
        (
         0.0,
         0.0,
         0.0,
         0.0,
         1.0,
         0.0,
         0.0,
         0.0
        );
    left_ev[entropy] = (double8)
        (
         1.0,
         0.0,
         0.0,
         0.0,
         0.0,
         0.0,
         0.0,
         -1.0/(a*a)
        );


    right_ev[minus_Cf] = (double8)
        (
         q.S0*alpha_f,
         -1.0*alpha_f*Cf,
         alpha_s*Cs*beta_y*sgnBx,
         alpha_s*Cs*beta_z*sgnBx,
         0.0,
         alpha_s*sqrt(q.S0)*a*beta_y,
         alpha_s*sqrt(q.S0)*a*beta_z,
         alpha_f*gamma*q.S7
        );
    right_ev[plus_Cf] = (double8)
        (
         right_ev[minus_Cf].S0,
         -right_ev[minus_Cf].S1,
         -right_ev[minus_Cf].S2,
         -right_ev[minus_Cf].S3,
         right_ev[minus_Cf].S4,
         right_ev[minus_Cf].S5,
         right_ev[minus_Cf].S6,
         right_ev[minus_Cf].S7
        );
    right_ev[minus_Ca] = (double8)
        (
         left_ev[minus_Ca].S0,
         left_ev[minus_Ca].S1,
         left_ev[minus_Ca].S2,
         left_ev[minus_Ca].S3,
         left_ev[minus_Ca].S4,
         -1.0*sqrt(q.S0/2.0)*beta_z,
         sqrt(q.S0/2.0)*beta_y,
         left_ev[minus_Ca].S7
        );
    right_ev[plus_Ca] = (double8)
        (
         right_ev[minus_Ca].S0,
         right_ev[minus_Ca].S1,
         right_ev[minus_Ca].S2,
         right_ev[minus_Ca].S3,
         right_ev[minus_Ca].S4,
         -rith_ev[minus_Ca].S5,
         -rith_ev[minus_Ca].S6,
         right_ev[minus_Ca].S7
        );
    right_ev[minus_Cs] = (double8)
        (
         q.S0*alpha_s,
         -1.0*alpha_s*Cs,
         -1.0*alpha_f*Cf*beta_y*sgnBx,
         -1.0*alpha_f*Cf*beta_z*sgnBx,
         0.0,
         alpha_f*sqrt(q.S0)*a*beta_y,
         alpha_f*sqrt(q.S0)*a*beta_z,
         alpha_s*gamma*q.S7
        );
    right_ev[plus_Cs] = (double8)
        (
         right_ev[minus_Cs].S0,
         -right_ev[minus_Cs].S1,
         -right_ev[minus_Cs].S2,
         -right_ev[minus_Cs].S3,
         right_ev[minus_Cs].S4,
         right_ev[minus_Cs].S5,
         right_ev[minus_Cs].S6,
         right_ev[minus_Cs].S7
        );
    right_ev[divergence] = left_ev[divergence];
    right_ev[entropy] = (double8)
        (
         1.0,
         0.0,
         0.0,
         0.0,
         0.0,
         0.0,
         0.0,
         0.0
        );
}



void recons_plm(
    __global double* const grid,        // Input grid
    __private double8* prim,            // Output reconstructed cell U vectors
    double dt
    double dx
    int grid_size,                      // Size of total grid
    int cell_num                        // num of given cell
    )
{
    double8 prim_q[3];

    double8 right_ev[8];
    double8 left_ev[8];
    double8 eigenvalues;

    double8 dq[3];

    derived_primitive_value(grid, prim_q, cell_num);    //convert U to q[3]
    eigenvalues = get_eigenvalues(prim_q[1]);
    get_eigenvectors(prim_q[1], left_ev, right_ev, eigenvalues);

    dq[0] = prim_q[1] - prim_q[0];              // dq_L (left)
    dq[1] = (prim_q[2] - prim_q[0])/2.0;        // dq_C (center)
    dq[2] = prim_q[2] - prim_q[1];              // dq_R (right)

    double8 da[3];
    da[0].S0 = dot_product(left_ev[0], dq[0])   // L(8*8) * dq_L(8*1) (see stone, 37.1)
    da[0].S1 = dot_product(left_ev[1], dq[0])
    da[0].S2 = dot_product(left_ev[2], dq[0])
    da[0].S3 = dot_product(left_ev[3], dq[0])
    da[0].S4 = dot_product(left_ev[4], dq[0])
    da[0].S5 = dot_product(left_ev[5], dq[0])
    da[0].S6 = dot_product(left_ev[6], dq[0])
    da[0].S7 = dot_product(left_ev[7], dq[0])

    da[1].S0 = dot_product(left_ev[0], dq[1])   // L(8*8) * dq_C(8*1) (see stone, 37.3)
    da[1].S1 = dot_product(left_ev[1], dq[1])
    da[1].S2 = dot_product(left_ev[2], dq[1])
    da[1].S3 = dot_product(left_ev[3], dq[1])
    da[1].S4 = dot_product(left_ev[4], dq[1])
    da[1].S5 = dot_product(left_ev[5], dq[1])
    da[1].S6 = dot_product(left_ev[6], dq[1])
    da[1].S7 = dot_product(left_ev[7], dq[1])

    da[2].S0 = dot_product(left_ev[0], dq[2])   // L(8*8) * dq_R(8*1) (see stone, 37.2)
    da[2].S1 = dot_product(left_ev[1], dq[2])
    da[2].S2 = dot_product(left_ev[2], dq[2])
    da[2].S3 = dot_product(left_ev[3], dq[2])
    da[2].S4 = dot_product(left_ev[4], dq[2])
    da[2].S5 = dot_product(left_ev[5], dq[2])
    da[2].S6 = dot_product(left_ev[6], dq[2])
    da[2].S7 = dot_product(left_ev[7], dq[2])

    double8 dam_tmp1 = (double8)                            // sgn(da[1])
        (
        sgn(da[1].S0),
        sgn(da[1].S1),
        sgn(da[1].S2),
        sgn(da[1].S3),
        sgn(da[1].S4),
        sgn(da[1].S5),
        sgn(da[1].S6),
        sgn(da[1].S7)
        );

    da[0] = (double8)                                       // 2|da_L|
        (
        2.0*fabs(da[0].S0),
        2.0*fabs(da[0].S1),
        2.0*fabs(da[0].S2),
        2.0*fabs(da[0].S3),
        2.0*fabs(da[0].S4),
        2.0*fabs(da[0].S5),
        2.0*fabs(da[0].S6),
        2.0*fabs(da[0].S7)
        );

    da[1] = (double8)                                       // |da_C|
        (
        fabs(da[1].S0),
        fabs(da[1].S1),
        fabs(da[1].S2),
        fabs(da[1].S3),
        fabs(da[1].S4),
        fabs(da[1].S5),
        fabs(da[1].S6),
        fabs(da[1].S7)
        );

    da[2] = (double8)                                       // 2|da_R|
        (
        2.0*fabs(da[2].S0),
        2.0*fabs(da[2].S1),
        2.0*fabs(da[2].S2),
        2.0*fabs(da[2].S3),
        2.0*fabs(da[2].S4),
        2.0*fabs(da[2].S5),
        2.0*fabs(da[2].S6),
        2.0*fabs(da[2].S7)
        );

    double8 dam_tmp2 = (double8)
        (
        fmin(da[0].S0, da[1].S0, da[2].S0),
        fmin(da[0].S1, da[1].S1, da[2].S1),
        fmin(da[0].S2, da[1].S2, da[2].S2),
        fmin(da[0].S3, da[1].S3, da[2].S3),
        fmin(da[0].S4, da[1].S4, da[2].S4),
        fmin(da[0].S5, da[1].S5, da[2].S5),
        fmin(da[0].S6, da[1].S6, da[2].S6),
        fmin(da[0].S7, da[1].S7, da[2].S7)
        );

    double8 dam = (double8)                                // stone, 38
        (
        dam_tmp1.S0 * dam_tmp2.S0,
        dam_tmp1.S1 * dam_tmp2.S1,
        dam_tmp1.S2 * dam_tmp2.S2,
        dam_tmp1.S3 * dam_tmp2.S3,
        dam_tmp1.S4 * dam_tmp2.S4,
        dam_tmp1.S5 * dam_tmp2.S5,
        dam_tmp1.S6 * dam_tmp2.S6,
        dam_tmp1.S7 * dam_tmp2.S7
        );

    double8 dwm = (double8)                                // stone, 39
        (
        dot_product(dam, right_ev[0]),
        dot_product(dam, right_ev[1]),
        dot_product(dam, right_ev[2]),
        dot_product(dam, right_ev[3]),
        dot_product(dam, right_ev[4]),
        dot_product(dam, right_ev[5]),
        dot_product(dam, right_ev[6]),
        dot_product(dam, right_ev[7])
        );

    //need global minimum dt, dx.
    //where is it?

    double maximum_eigenvalue = get_max_double8(eigenvalues);
    double minumum_eigenvalue = get_min_double8(eigenvalues);

    double8 ql_ref = (double8)                             // stone, 40
        (
        prim_q[1].S0 + ( 0.5 - fmax(maximum_eigenvalue, 0.0) * (dt/(2.0*dx)) ) * dwm.S0,
        prim_q[1].S1 + ( 0.5 - fmax(maximum_eigenvalue, 0.0) * (dt/(2.0*dx)) ) * dwm.S1,
        prim_q[1].S2 + ( 0.5 - fmax(maximum_eigenvalue, 0.0) * (dt/(2.0*dx)) ) * dwm.S2,
        prim_q[1].S3 + ( 0.5 - fmax(maximum_eigenvalue, 0.0) * (dt/(2.0*dx)) ) * dwm.S3,
        prim_q[1].S4 + ( 0.5 - fmax(maximum_eigenvalue, 0.0) * (dt/(2.0*dx)) ) * dwm.S4,
        prim_q[1].S5 + ( 0.5 - fmax(maximum_eigenvalue, 0.0) * (dt/(2.0*dx)) ) * dwm.S5,
        prim_q[1].S6 + ( 0.5 - fmax(maximum_eigenvalue, 0.0) * (dt/(2.0*dx)) ) * dwm.S6,
        prim_q[1].S7 + ( 0.5 - fmax(maximum_eigenvalue, 0.0) * (dt/(2.0*dx)) ) * dwm.S7
        );

    double8 qr_ref = (double8)                             // stone, 41
        (
        prim_q[1].S0 + ( 0.5 - fmin(minimum_eigenvalue, 0.0) * (dt/(2.0*dx)) ) * dwm.S0,
        prim_q[1].S1 + ( 0.5 - fmin(minimum_eigenvalue, 0.0) * (dt/(2.0*dx)) ) * dwm.S1,
        prim_q[1].S2 + ( 0.5 - fmin(minimum_eigenvalue, 0.0) * (dt/(2.0*dx)) ) * dwm.S2,
        prim_q[1].S3 + ( 0.5 - fmin(minimum_eigenvalue, 0.0) * (dt/(2.0*dx)) ) * dwm.S3,
        prim_q[1].S4 + ( 0.5 - fmin(minimum_eigenvalue, 0.0) * (dt/(2.0*dx)) ) * dwm.S4,
        prim_q[1].S5 + ( 0.5 - fmin(minimum_eigenvalue, 0.0) * (dt/(2.0*dx)) ) * dwm.S5,
        prim_q[1].S6 + ( 0.5 - fmin(minimum_eigenvalue, 0.0) * (dt/(2.0*dx)) ) * dwm.S6,
        prim_q[1].S7 + ( 0.5 - fmin(minimum_eigenvalue, 0.0) * (dt/(2.0*dx)) ) * dwm.S7
        );

    double8 left_sum = (double8)
        (
        0.0,
        0.0,
        0.0,
        0.0,
        0.0,
        0.0,
        0.0,
        0.0
        );

    double8 right_sum = (double8)
        (
        0.0,
        0.0,
        0.0,
        0.0,
        0.0,
        0.0,
        0.0,
        0.0
        );

    


    double8 advanced_left = (double8)
        (
        ql_ref.S0 + (dt/(2.0*dx)) * left_sum.S0,
        ql_ref.S1 + (dt/(2.0*dx)) * left_sum.S1,
        ql_ref.S2 + (dt/(2.0*dx)) * left_sum.S2,
        ql_ref.S3 + (dt/(2.0*dx)) * left_sum.S3,
        ql_ref.S4 + (dt/(2.0*dx)) * left_sum.S4,
        ql_ref.S5 + (dt/(2.0*dx)) * left_sum.S5,
        ql_ref.S6 + (dt/(2.0*dx)) * left_sum.S6,
        ql_ref.S7 + (dt/(2.0*dx)) * left_sum.S7
        );

    double8 advanced_right = (double8)
        (
        qr_ref.S0 + (dt/(2.0*dx)) * right_sum.S0,
        qr_ref.S1 + (dt/(2.0*dx)) * right_sum.S1,
        qr_ref.S2 + (dt/(2.0*dx)) * right_sum.S2,
        qr_ref.S3 + (dt/(2.0*dx)) * right_sum.S3,
        qr_ref.S4 + (dt/(2.0*dx)) * right_sum.S4,
        qr_ref.S5 + (dt/(2.0*dx)) * right_sum.S5,
        qr_ref.S6 + (dt/(2.0*dx)) * right_sum.S6,
        qr_ref.S7 + (dt/(2.0*dx)) * right_sum.S7
        );









}

__kernel void mhd_solver(
    __global double* grid,
    __global double* grid_advance,
    __global double* smax_inverse,
    __global double* dt,
    __global double* dx,
    int grid_size
    )
{
    int tid = get_global_id(0);
    if(tid >= grid_size) {return; }

    private double8 prim_q[3];
    private double8 left_ev[8];
    private double8 right_ev[8];
    private double8 eigenvalues;
    private double8 advanced_left;
    private double8 advanced_right;
    private double8 advancedFlux[2];

    derived_primitive_value(grid, prim_q, tid);

    smax_inverse[tid] = get_smax_inverse(prim_q[1]);
    //send smax_inverse to master node
    //syncThreads
    //get global minimum dt

    get_eigenvectors(prim_q[1], left_ev, right_ev, eigenvalues);

    //@TODO : Piecewise linear reconstruction
    //        Input(eigenvalues, left_ev, right_ev, prim_q, dt, dx)
    //        Output(advanced_right, advanced_left)

    //@TODO : convert prim_q to U
    //        Input(advanced_right, advanced_left)
    //        Output(advanced_right, advanced_left)

    recons_grid[(tid*2*8) + 0] = advanced_right.S0;
    recons_grid[(tid*2*8) + 1] = advanced_right.S1;
    recons_grid[(tid*2*8) + 2] = advanced_right.S2;
    recons_grid[(tid*2*8) + 3] = advanced_right.S3;
    recons_grid[(tid*2*8) + 4] = advanced_right.S4;
    recons_grid[(tid*2*8) + 5] = advanced_right.S5;
    recons_grid[(tid*2*8) + 6] = advanced_right.S6;
    recons_grid[(tid*2*8) + 7] = advanced_right.S7;

    recons_grid[(tid*2-1)*8 + 0] = advanced_left.S0;
    recons_grid[(tid*2-1)*8 + 1] = advanced_left.S1;
    recons_grid[(tid*2-1)*8 + 2] = advanced_left.S2;
    recons_grid[(tid*2-1)*8 + 3] = advanced_left.S3;
    recons_grid[(tid*2-1)*8 + 4] = advanced_left.S4;
    recons_grid[(tid*2-1)*8 + 5] = advanced_left.S5;
    recons_grid[(tid*2-1)*8 + 6] = advanced_left.S6;
    recons_grid[(tid*2-1)*8 + 7] = advanced_left.S7;

    //syncThreads

    //@TODO : HLL Riemann solver
    //        Input(recons_grid[(tid-1)*2], recons_grid[(tid)*2-1], prim_q)
    //                      Ul                      Ur               Sr,Sl
    //        Output(advancedFlux[0])
    //                  left flux

    //@TODO : HLL Riemann solver
    //        Input(recons_grid[(tid)*2], recons_grid[(tid+1)*2-1], prim_q)
    //                      Ul                      Ur               Sr,Sl
    //        Output(advancedFlux[1])
    //                  right flux

    //@TODO : integration

}

