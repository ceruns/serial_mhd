#pragma once
#include <utility>
#define __NO_STD_VECTOR // Use cl::vector instead of STL version
#define __USE_GPU
#include <CL/cl.hpp>

#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>
#include <iterator>

const size_t ARRAY_SIZE = 256;
const size_t NUM_OF_EQ = 8;
const double grid_real_size = 1.0;
const double cfl_number = 0.475;


inline void
checkErr(cl_int err, const char *name)
{
    if (err != CL_SUCCESS)
    {
        std::cerr << "ERROR: " << name
                  << " (" << err << ")" << std::endl;
        //exit(EXIT_FAILURE);
    }
}

int main(void)
{
    //FIND Context
    cl_int err;
    cl::vector< cl::Platform > platformList;
    cl::Platform::get(&platformList);
    checkErr(platformList.size() != 0 ? CL_SUCCESS : -1, "cl::Platform::get");
    //GET Platform
    std::cerr << "Platform number is: " << platformList.size() << std::endl;
    std::string platformVendor;
    platformList[0].getInfo((cl_platform_info)CL_PLATFORM_VENDOR, &platformVendor);
    std::cerr << "Platform is by: " << platformVendor << "\n";

    cl_context_properties cprops[3] =
    {
        CL_CONTEXT_PLATFORM,
        (cl_context_properties)(platformList[0])(),
        0
    };
    cl::Context context;
#ifdef __USE_GPU
    context = cl::Context(
                  CL_DEVICE_TYPE_GPU,
                  cprops,
                  NULL,
                  NULL,
                  &err);
#else
    context = cl::Context(
                  CL_DEVICE_TYPE_CPU,
                  cprops,
                  NULL,
                  NULL,
                  &err);
#endif

    checkErr(err, "Context::Context()");

    //CREATE Buffer
    double U_vectors[ARRAY_SIZE * NUM_OF_EQ];
    double advance_vectors[ARRAY_SIZE * NUM_OF_EQ];
    double smax_inverse[ARRAY_SIZE];
    int grid_size = (int)ARRAY_SIZE;

    for (int i = 0; i < ARRAY_SIZE / 2 * NUM_OF_EQ;)
    {
        U_vectors[i++] = 1.0;
        U_vectors[i++] = 0.0;
        U_vectors[i++] = 0.0;
        U_vectors[i++] = 0.0;
        U_vectors[i++] = 1.0;
        U_vectors[i++] = 1.0;
        U_vectors[i++] = 0.0;
        U_vectors[i++] = 1.0;
    }
    for (int i = ARRAY_SIZE / 2 * NUM_OF_EQ; i < ARRAY_SIZE * NUM_OF_EQ; )
    {
        U_vectors[i++] = 0.125;
        U_vectors[i++] = 0.0;
        U_vectors[i++] = 0.0;
        U_vectors[i++] = 0.0;
        U_vectors[i++] = 1.0;
        U_vectors[i++] = -1.0;
        U_vectors[i++] = 0.0;
        U_vectors[i++] = 0.1;
    }

    for (int i = 0; i < ARRAY_SIZE * NUM_OF_EQ;)
    {
        std::cout << i << "th cell: ";
        std::cout << U_vectors[i++];
        std::cout << U_vectors[i++];
        std::cout << U_vectors[i++];
        std::cout << U_vectors[i++];
        std::cout << U_vectors[i++];
        std::cout << U_vectors[i++];
        std::cout << U_vectors[i++];
        std::cout << U_vectors[i++];
        std::cout << std::endl;
    }

    cl::Buffer U_buffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, ARRAY_SIZE * NUM_OF_EQ * sizeof(double), U_vectors, &err);
    checkErr(err, "Buffer::Buffer()");
    cl::Buffer advance_buffer(context, CL_MEM_READ_WRITE, ARRAY_SIZE * NUM_OF_EQ * sizeof(double), advance_vectors, &err);
    checkErr(err, "Buffer::Buffer()");
    cl::Buffer smax_buffer(context, CL_MEM_READ_WRITE, ARRAY_SIZE * NUM_OF_EQ * sizeof(double), smax_inverse, &err);
    checkErr(err, "Buffer::Buffer()");


    //GET Target DEVICE
    cl::vector<cl::Device> devices;
    devices = context.getInfo<CL_CONTEXT_DEVICES>();
    checkErr(devices.size() > 0 ? CL_SUCCESS : -1, "devices.size() > 0");
    std::cout << "Device size : " << devices.size() << std::endl;

    //CREATE Program
    std::ifstream mhd_file("mhd_kernel.cl");
    checkErr(mhd_file.is_open() ? CL_SUCCESS : -1, "mhd_kernel.cl");
    std::string mhd_prog(std::istreambuf_iterator<char>(mhd_file), (std::istreambuf_iterator<char>()));
    cl::Program::Sources mhd_source(1, std::make_pair(mhd_prog.c_str(), mhd_prog.length() + 1));
    cl::Program mhd_program(context, mhd_source);
    //BUILD Program
    err = mhd_program.build(devices, "");
    checkErr(err, "Program::build()");


    //SHOWOFF Build Log
    if (err != CL_SUCCESS)
    {
        char buildlog[0x1000];
        mhd_program.getBuildInfo( devices[0], (cl_program_build_info)CL_PROGRAM_BUILD_LOG, &buildlog );
        std::cout << buildlog << std::endl;
    }


    //SELECT Kernel
    cl::Kernel mhd_kernel(mhd_program, "mhd_solver", &err);
    checkErr(err, "Kernel::Kernel()");



    //SET mhd_kernel's Arguments
    err = mhd_kernel.setArg(0, U_buffer);
    err |= mhd_kernel.setArg(1, advance_buffer);
    err |= mhd_kernel.setArg(2, smax_buffer);
    err |= mhd_kernel.setArg(3, grid_size);
    checkErr(err, "Kernel::setArg()");

    //CREATE another Commmand Queue
    cl::CommandQueue mhd_queue(context, devices[0], 0, &err);
    checkErr(err, "CommandQueue::CommandQueue()");
    cl::Event mhd_event;

    double cumm_time = 0.0;

    for (int iterator = 0; iterator < 10000 ; iterator++)
    {
        //EXECUTE MHD
        err = mhd_queue.enqueueNDRangeKernel(mhd_kernel, cl::NullRange, cl::NDRange(ARRAY_SIZE), cl::NDRange(1, 1), NULL, &mhd_event);
        checkErr(err, "ComamndQueue::enqueueNDRangeKernel()");

        //READ mhd_result
        mhd_event.wait();
        err = mhd_queue.enqueueReadBuffer(advance_buffer, CL_TRUE, 0, ARRAY_SIZE * NUM_OF_EQ * sizeof(double), advance_vectors);
        checkErr(err, "Buffer::read() advance_vectors");
        err = mhd_queue.enqueueReadBuffer(smax_buffer, CL_TRUE, 0, ARRAY_SIZE * sizeof(double), smax_inverse);
        checkErr(err, "Buffer::read() smax_inverse");
        err = mhd_queue.enqueueReadBuffer(U_buffer, CL_TRUE, 0, ARRAY_SIZE * NUM_OF_EQ * sizeof(double), U_vectors);
        checkErr(err, "Buffer::read() U_vectors");

        double min_smax = 100000.0;
        for (int i = 0; i < ARRAY_SIZE; ++i)
        {
            min_smax = std::min(min_smax, smax_inverse[i]);
        }
        cumm_time += min_smax;
        for (int i = 0; i < ARRAY_SIZE * NUM_OF_EQ; i++)
        {
            U_vectors[i] = U_vectors[i] + min_smax * cfl_number * grid_real_size / ARRAY_SIZE * advance_vectors[i];
        }
        mhd_queue.enqueueWriteBuffer(U_buffer, CL_TRUE, 0, ARRAY_SIZE * NUM_OF_EQ * sizeof(double), U_vectors);

        if (iterator % 1000 == 0)
        {
            std::cout<< "Current time : "<<cumm_time<<std::endl;
            for (int print_index = 0; print_index < ARRAY_SIZE * NUM_OF_EQ;)
            {
                std::cout << print_index << "th cell: ";
                std::cout << U_vectors[print_index++] << ",";
                std::cout << U_vectors[print_index++] << ",";
                std::cout << U_vectors[print_index++] << ",";
                std::cout << U_vectors[print_index++] << ",";
                std::cout << U_vectors[print_index++] << ",";
                std::cout << U_vectors[print_index++] << ",";
                std::cout << U_vectors[print_index++] << ",";
                std::cout << U_vectors[print_index++] << ",";
                std::cout << std::endl;
            }
        }
    }


    return EXIT_SUCCESS;
}


