#define __USE_GPU
#define __STDOUT_RESULT
//#define __DEBUG

#include <utility>
#include <CL/cl.hpp>

#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>
#include <iterator>

const size_t ARRAY_SIZE = 1600;
const size_t NUM_OF_EQ = 8;
const double grid_real_size = 1.0;
const double cfl_number = 0.475;
const size_t max_step = 2000000;
const double target_time = 0.2;
const double dx = grid_real_size / (double)ARRAY_SIZE;
const size_t PRINT_EVERY = 100;

inline void checkErr(cl_int err, const char *name)
{
    if (err != CL_SUCCESS)
    {
        std::cerr << "ERROR: " << name
                  << " (" << err << ")" << std::endl;
        exit(EXIT_FAILURE);
    }
}

int main(int agvc, char *args[])
{
    //FIND Context
    cl_int err;
    std::vector< cl::Platform > platformList;
    cl::Platform::get(&platformList);
    checkErr(platformList.size() != 0 ? CL_SUCCESS : -1, "cl::Platform::get");
    //GET Platform
    std::cerr << "Platform number is: " << platformList.size() << std::endl;
    std::string platformVendor;
    size_t best_platform_id = 0;
    for (size_t i = 0; i < platformList.size(); i++)
    {
        platformList[i].getInfo((cl_platform_info)CL_PLATFORM_VENDOR, &platformVendor);
        std::cerr << i << "th Platform is by: " << platformVendor << "\n";
        if (platformVendor.find("Advanced Micro Devices") != std::string::npos || platformVendor.find("NVIDIA") != std::string::npos)
        {
            best_platform_id = i;
            std::cerr << "found best platform!: " << platformVendor << std::endl;
        }
    }
    platformList[best_platform_id].getInfo((cl_platform_info)CL_PLATFORM_VENDOR, &platformVendor);



    cl_context_properties cprops[3] =
    {
        CL_CONTEXT_PLATFORM,
        (cl_context_properties)(platformList[best_platform_id])(),
        0
    };
    cl::Context context;
    context = cl::Context(CL_DEVICE_TYPE_GPU, cprops, NULL, NULL, &err);
    if (platformVendor.find("Intel") != std::string::npos)
    {
        context = cl::Context(CL_DEVICE_TYPE_CPU, cprops, NULL, NULL, &err);
    }
    else
    {
        context = cl::Context(CL_DEVICE_TYPE_GPU, cprops, NULL, NULL, &err);
    }

    checkErr(err, "Context::Context()");

    //CREATE Buffer
    double U_vectors[ARRAY_SIZE * NUM_OF_EQ];
    double advance_vectors[ARRAY_SIZE * NUM_OF_EQ];
    double debugging_vectors[ARRAY_SIZE * NUM_OF_EQ];
    cl_double8 interface_vectors[ARRAY_SIZE];
    double smax_inverse[ARRAY_SIZE];
    int grid_size = (int)ARRAY_SIZE;

    for (int i = 0; i <= ARRAY_SIZE / 2 * NUM_OF_EQ;)
    {
        U_vectors[i++] = 1.0;
        U_vectors[i++] = 1.0;
        U_vectors[i++] = 0.0;
        U_vectors[i++] = 0.0;
        U_vectors[i++] = 1.0;
        U_vectors[i++] = 1.0;
        U_vectors[i++] = 0.0;
        U_vectors[i++] = 2.5;
    }
    for (int i = ARRAY_SIZE / 2 * NUM_OF_EQ; i < ARRAY_SIZE * NUM_OF_EQ; )
    {
        U_vectors[i++] = 0.125;
        U_vectors[i++] = 0.0;
        U_vectors[i++] = 0.0;
        U_vectors[i++] = 0.0;
        U_vectors[i++] = 1.0;
        U_vectors[i++] = -1.0;
        U_vectors[i++] = 0.0;
        U_vectors[i++] = 1.15;
    }

    cl::Buffer U_buffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, ARRAY_SIZE * NUM_OF_EQ * sizeof(double), U_vectors, &err);
    checkErr(err, "U Buffer::Buffer()");
    cl::Buffer advance_buffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, ARRAY_SIZE * NUM_OF_EQ * sizeof(double), advance_vectors, &err);
    checkErr(err, "Adv Buffer::Buffer()");
    cl::Buffer debugging_buffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, ARRAY_SIZE * NUM_OF_EQ * sizeof(double), debugging_vectors, &err);
    checkErr(err, "Adv Buffer::Buffer()");
    cl::Buffer interface_buffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, ARRAY_SIZE * 2 * sizeof(cl_double8), interface_vectors, &err);
    checkErr(err, "Adv Buffer::Buffer()");
    cl::Buffer smax_buffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, ARRAY_SIZE * sizeof(double), smax_inverse, &err);
    checkErr(err, "smax Buffer::Buffer()");

    //GET Target DEVICE
    std::vector<cl::Device> devices;
    devices = context.getInfo<CL_CONTEXT_DEVICES>();
    checkErr(devices.size() > 0 ? CL_SUCCESS : -1, "devices.size() > 0");
    std::cout << "Device size : " << devices.size() << std::endl;

    //CREATE Program
    std::ifstream mhd_file("kernel/mhd_kernel.cl");
    checkErr(mhd_file.is_open() ? CL_SUCCESS : -1, "kernel/mhd_kernel.cl");
    std::string mhd_prog(std::istreambuf_iterator<char>(mhd_file), (std::istreambuf_iterator<char>()));
    cl::Program::Sources mhd_source(1, std::make_pair(mhd_prog.c_str(), mhd_prog.length() + 1));
    cl::Program mhd_program(context, mhd_source);
    //BUILD Program
    err = mhd_program.build(devices, "-I ./kernel/ ");
    //SHOWOFF Build Log
    if (err != CL_SUCCESS)
    {
        char buildlog[0x1000];
        mhd_program.getBuildInfo( devices[0], (cl_program_build_info)CL_PROGRAM_BUILD_LOG, &buildlog );
        std::cerr << buildlog << std::endl;
    }

    checkErr(err, "Program::build()");

    //SELECT Kernel
    cl::Kernel mhd_kernel(mhd_program, "mhd_plm_hll", &err);
    checkErr(err, "MHD Kernel::Kernel()");
    cl::Kernel smax_kernel(mhd_program, "mhd_smax_inverse", &err);
    checkErr(err, "SMAX Kernel::Kernel()");


    //SET mhd_kernel's Arguments
    err = mhd_kernel.setArg(0, U_buffer);
    err |= mhd_kernel.setArg(1, advance_buffer);
    err |= mhd_kernel.setArg(2, smax_buffer);
    err |= mhd_kernel.setArg(3, grid_size);
    err |= mhd_kernel.setArg(4, 0.0000000001);
    err |= mhd_kernel.setArg(5, dx);
    err |= mhd_kernel.setArg(6, interface_buffer);
    err |= mhd_kernel.setArg(7, debugging_buffer);
    checkErr(err, "MHD Kernel::setArg()");

    err = smax_kernel.setArg(0, U_buffer);
    err |= smax_kernel.setArg(1, smax_buffer);
    checkErr(err, "SMAX Kernel::setArg()");

    //CREATE another Commmand Queue
    cl::CommandQueue mhd_queue(context, devices[0], 0, &err);
    checkErr(err, "CommandQueue::CommandQueue()");
    cl::Event mhd_event;
    cl::NDRange localSize = cl::NullRange;
    cl::NDRange globalSize = cl::NDRange(ARRAY_SIZE);

    double cumm_time = 0.0;
    double dt = 0.0;
    std::cerr << "The initial state, current time:  \t" << cumm_time << std::endl;
    for (int element_index = 0; element_index < 8; element_index++)
    {
        for (int print_index = element_index; print_index < ARRAY_SIZE * NUM_OF_EQ;)
        {
            std::cerr << U_vectors[print_index] << "\t";
            print_index += 8;
        }
        std::cout << std::endl;
    }
    for (int iterator = 1; cumm_time < target_time && iterator < max_step ; iterator++)
    {
        //Execute Smax Kernel and get dt
        err = mhd_queue.enqueueNDRangeKernel(smax_kernel, cl::NullRange, globalSize, localSize, NULL, &mhd_event);
        mhd_event.wait();
        checkErr(err, "CommandQueue::enqueueNDRangeKernel,SMAX Kernel");
        err = mhd_queue.enqueueReadBuffer(smax_buffer, CL_TRUE, 0, ARRAY_SIZE * sizeof(double), smax_inverse);
        double min_smax_prior = 100000.0;
        for (int i = 1; i < ARRAY_SIZE-1; ++i)
        {
            min_smax_prior = std::min(min_smax_prior, smax_inverse[i]);
        }
        dt =  cfl_number * min_smax_prior * dx;
        err = mhd_kernel.setArg(4, dt);
        checkErr(err, "Setting dt");
#ifdef __DEBUG
        std::cerr << "Current prior dt: " << dt << "\t Now, trigger MHD solver" << std::endl;
#endif
        //EXECUTE MHD
        err = mhd_queue.enqueueNDRangeKernel(mhd_kernel, cl::NullRange, globalSize, localSize, NULL, &mhd_event);
        checkErr(err, "ComamndQueue::enqueueNDRangeKernel, MHD Kenrel");

        //READ mhd_result
        mhd_event.wait();
        err = mhd_queue.enqueueReadBuffer(advance_buffer, CL_TRUE, 0, ARRAY_SIZE * NUM_OF_EQ * sizeof(double), advance_vectors);
        checkErr(err, "Buffer::read() advance_vectors");
        err = mhd_queue.enqueueReadBuffer(smax_buffer, CL_TRUE, 0, ARRAY_SIZE * sizeof(double), smax_inverse);
        checkErr(err, "Buffer::read() smax_inverse");

#ifdef  __DEBUG
        err = mhd_queue.enqueueReadBuffer(debugging_buffer, CL_TRUE, 0, ARRAY_SIZE * NUM_OF_EQ * sizeof(double), debugging_vectors);
        checkErr(err, "Buffer::read() debugging_vectors");
        std::cerr << std::endl << "Debugging vectors" << std::endl;
        for (int print_index = 0; print_index < ARRAY_SIZE * NUM_OF_EQ;)
        {
            std::cerr << debugging_vectors[print_index] << "\t";
            print_index ++;
            if (print_index % 8 == 0)
                std::cerr << std::endl;
        }
#endif

        double min_smax = 100000.0;
        for (int i = 1; i < ARRAY_SIZE-1; ++i)
        {
            min_smax = std::min(min_smax, smax_inverse[i]);
        }
        dt = cfl_number * min_smax * dx;
        cumm_time += dt;

        size_t break_checker = 0;
        for (int i = 2 * 8; i < (ARRAY_SIZE - 2) * NUM_OF_EQ; i++)
        {
            U_vectors[i] = U_vectors[i] + (dt / ( dx)) * advance_vectors[i];
#ifdef __DEBUG

            if ( i % 8 == 0 )
            {
                if (U_vectors[i] < 0.0 || U_vectors[i] == 0.0)
                {
                    std::cerr << "Current denstiy at cell " << i / 8 << "\t" << U_vectors[i] << std::endl;
                    std::cerr << "Minus density!" << std::endl;
                    break_checker++;
                }
            }
        }

        if (break_checker > 0)
            break;
#else
        }
#endif

        mhd_queue.enqueueWriteBuffer(U_buffer, CL_TRUE, 0, ARRAY_SIZE * NUM_OF_EQ * sizeof(double), U_vectors);

#ifdef __STDOUT_RESULT
        if (iterator % PRINT_EVERY == 0)
        {
            std::cout << "$t: " << cumm_time << "$     $step: " << iterator << "$     $dt: " << dt << "$" << std::endl;
            for (int element_index = 0; element_index < 8; element_index++)
            {
                for (int print_index = element_index; print_index < ARRAY_SIZE * NUM_OF_EQ;)
                {
                    std::cout << U_vectors[print_index] << "\t";
                    print_index += 8;
                }
                std::cout << std::endl;
            }
            std::cerr << "dt: \t" << dt << std::endl;
            std::cerr << std::endl;
        }
#endif

    }
#ifdef __STDOUT_RESULT

    //The last result
    err = mhd_queue.enqueueReadBuffer(U_buffer, CL_TRUE, 0, ARRAY_SIZE *NUM_OF_EQ *sizeof(double), U_vectors);
    checkErr(err, "Buffer::read() U_vectors");
    std::cerr << "The last result, current time:  \t" << cumm_time << std::endl;
    for (int element_index = 0; element_index < 8; element_index++)
    {
        for (int print_index = element_index; print_index < ARRAY_SIZE * NUM_OF_EQ;)
        {
            std::cout << U_vectors[print_index] << "\t";
            print_index += 8;
        }
        std::cout << std::endl;
    }
    std::cerr << "dt: \t" << dt << std::endl;
    std::cerr << std::endl;
    std::cerr << std::endl;
    #endif

    std::cerr << "Simulation_All_DONE!" << std::endl;

    return EXIT_SUCCESS;
}


