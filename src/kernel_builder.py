import pyopencl as cl

context = cl.create_some_context()
queue = cl.CommandQueue(context)
with open('./kernel/mhd_kernel.cl','r') as f:
	kernelSource = f.read()

program = cl.Program(context, kernelSource).build("-I ./kernel/ -Werror")