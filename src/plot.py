#!/usr/bin/env python
import numpy as np
import pylab as py
import Tkinter as tk
from datetime import datetime

py.ion()

top = tk.Tk()
tex = tk.Text(master=top)
tex.pack(side=tk.RIGHT)
bop = tk.Frame()
bop.pack(side=tk.LEFT)
counter = 0
txt = '1D MHD viewer'

mng = py.get_current_fig_manager()
mng.resize(*mng.window.maxsize())
data = [0,0,0,0,0,0,0,0]
def plot_data(data, txt):
    if counter % 8 is 0:
        py.clf()
    py.xlabel('x')
    py.ylabel('value')
    for x in data:
        py.plot(x, marker='x',markersize=4, linewidth=1)

    py.legend('01234567')
    py.title(txt)
    py.draw()
    #py.savefig("data-%.8d.png"%counter)

if __name__ == '__main__':
    while True:
        try:
            tmp = raw_input()
            splt = tmp.strip().split()
            data[counter%8] = np.array(splt, dtype=np.double)
        except EOFError:
            print "Input has terminated! Exiting"
            py.pause(2**31 -1)
        except ValueError:
            txt = tmp
            tex.insert(tk.END,datetime.now().time())
            tex.insert(tk.END, txt)
            tex.insert(tk.END, "\n")
            tex.see(tk.END) # Scroll if necessary
            continue

        if counter%8 is 0:
            plot_data(data, txt)
            tex.insert(tk.END,datetime.now().time())
            tex.insert(tk.END," Plotting plot number %d "%counter)
            tex.insert(tk.END, "\n")
            tex.see(tk.END)
        counter +=1