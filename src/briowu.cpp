//#include "main.h"
#include "openmp.h"
#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <iterator>
#include <fstream>

#include "vectors.h"
#include "riemann.h"
#include "recons.h"
#include "integrator.h"
#include "visualizer.h"

#include "boundarycond.h" //initialize boundary condition

int main(int argc, char const *argv[])
{
	std::string ProblemName = "BRIO-WU";

	std::vector<U> * init_grid = new std::vector<U>;
	init_grid->reserve(CELL_SIZE);

	for (int i = 0; i < CELL_SIZE / 2; ++i)
	{
		init_grid->push_back(U(1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 1.0));
	}
	for (int i = CELL_SIZE / 2; i < CELL_SIZE; ++i)
	{
		init_grid->push_back(U(0.125, 0.0, 0.0, 0.0, 0.0, -1.0, 0.0, 0.1));
	}

	std::cout << "CELL INITIALIZED FOR " + ProblemName + " PROBLEM" << std::endl;
	for (int i = 0; i < CELL_SIZE; ++i)
	{
		init_grid->at(i).print();
	}
	
	std::vector<U> source_grid = *init_grid;
	std::vector<U> target_grid = source_grid; //first deep copy
	 	
	HLL RiemannSolver;
	EulerIntegrator Integrator(&source_grid, &target_grid);	
	PieceConst Reconstructor(&source_grid);
	
	visualizer Vis(source_grid, 0.0, DELTA_X, DELTA_T, OUTPUT_RESOL);  //visualizing with gnuplot every "OUTPUT_RESOL" steps

	for (int step = 0; step < TOTAL_STEP; step++)
	{
		if (step % OUTPUT_RESOL == 0)
		{
			std::cout << "Now " << (100.0*step) / TOTAL_STEP << "%" << std::endl;
			Vis.set_data_and_show(source_grid);
			Vis.save(ProblemName + "_");
		}
		
		#pragma omp parallel for
	 	for (int i = 1; i < CELL_SIZE - 1; ++i)	//Actual computation
	 	{
	 		Flux minus_half = RiemannSolver.getF(
	 			Reconstructor.get_minus_half_left(i),
	 			Reconstructor.get_minus_half_right(i));

	 		Flux plus_half = RiemannSolver.getF(
	 			Reconstructor.get_plus_half_left(i),
	 			Reconstructor.get_plus_half_right(i));

	 		Integrator.integrate(i, DELTA_T, DELTA_X, minus_half, plus_half);
	 	}
	 	#pragma omp barrier
	 	source_grid = target_grid; // Deep copy
	}
	return 0;
}
