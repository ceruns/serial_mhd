import pyopencl as cl
import pyopencl.array as cl_array
import numpy as np
import unittest

ocl_build_option = '-I ./kernel/ -w'

print('Version of pyopencl')
print(cl.get_cl_header_version())
context = cl.create_some_context()
ctx = context
queue = cl.CommandQueue(context)
mf = cl.mem_flags


class TestVectorUtil(unittest.TestCase):
	"""Test for vector_util.h"""
	def setUp(self):
		pass

	def test_get_max_double8(self):
		prg = cl.Program(ctx, """
		#include "vector_util.h"
		__kernel void test_vector_util_get_max_double8(__global double *res_g)
		{
			size_t pid = get_global_id(0);
			res_g[pid] = get_max_double8((double8)(1.0, 2.0, -3.0, 4.0, 5.0, 6.0, 7.0*(double)(pid+1), -8.0)) - 7.0*(double)(pid+1);
		}
		""").build(ocl_build_option)

		a_np = np.random.rand(5000).astype(np.float64)
		res_g = cl.Buffer(ctx, mf.WRITE_ONLY, a_np.nbytes)
		prg.test_vector_util_get_max_double8(queue, a_np.shape, None, res_g)
		res_np = np.empty_like(a_np)
		cl.enqueue_copy(queue, res_np, res_g)
		result = np.linalg.norm(res_np)
		self.assertEqual(result, 0.0)

	def test_get_min_double8(self):
		prg = cl.Program(ctx, """
			#include "vector_util.h"
			__kernel void test_vector_util_get_min_double8(__global double *res_g)
			{
				size_t pid = get_global_id(0);
				res_g[pid] = get_min_double8( (double8)(1.0, 2.0, -3.0*(double)(pid+1), 4.0, 5.0, 6.0, 7.0, 0.0)) + 3.0*(double)(pid+1);
			}
			""").build(ocl_build_option)
		a_np = np.random.rand(5000).astype(np.float64)
		res_g = cl.Buffer(ctx, mf.WRITE_ONLY, a_np.nbytes)
		prg.test_vector_util_get_min_double8(queue, a_np.shape, None, res_g)
		res_np = np.empty_like(a_np)
		cl.enqueue_copy(queue, res_np, res_g)
		result = np.linalg.norm(res_np)
		self.assertEqual(result, 0.0)

	def test_u_to_q(self):
		source = """
		#include "vector_util.h"
		__kernel void test_u_to_q(__global double *res_g)
		{
			size_t pid = get_global_id(0);
			
			double8 test_u1 = (double8) (1.0, 0.0, 0.0, 0.0, 1.0, 1.0, 0.0, 2.5);
			double8 test_u2 = (double8) (0.125, 0.0, 0.0, 0.0, 1.0, -1.0, 0.0, 0.15);
			double8 test_q1 = u_to_q(test_u1);
			double8 test_q2 = u_to_q(test_u2);
			
			res_g[pid] = 0.0;
			res_g[pid] += test_q1.s0 - 1.0;
			res_g[pid] += test_q1.s1 - 0.0;
			res_g[pid] += test_q1.s2 - 0.0;
			res_g[pid] += test_q1.s3 - 0.0;
			res_g[pid] += test_q1.s4 - 1.0;
			res_g[pid] += test_q1.s5 - 1.0;
			res_g[pid] += test_q1.s6 - 0.0;
			res_g[pid] += test_q1.s7 - 1.0;

			res_g[pid] += test_q2.s0 - 0.125;
			res_g[pid] += test_q2.s1 - 0.0;
			res_g[pid] += test_q2.s2 - 0.0;
			res_g[pid] += test_q2.s3 - 0.0;
			res_g[pid] += test_q2.s4 - 1.0;
			res_g[pid] += test_q2.s5 + 1.0;
			res_g[pid] += test_q2.s6 - 0.0;
			res_g[pid] += test_q2.s7 + 0.566666666666667;
			return;
		}
		"""
		prg = cl.Program(ctx, source).build(ocl_build_option)
		a_np = np.random.rand(1).astype(np.float64)
		res_g = cl.Buffer(ctx, mf.WRITE_ONLY, a_np.nbytes)
		prg.test_u_to_q(queue, a_np.shape, None, res_g)
		res_np = np.empty_like(a_np)
		cl.enqueue_copy(queue, res_np, res_g)
		result = np.linalg.norm(res_np)
		self.assertAlmostEqual(result, 0.0)
	
	def test_q_to_u(self):
		source = """
		#include "vector_util.h"
		__kernel void test_q_to_u(__global double *res_g)
		{
			size_t pid = get_global_id(0);
			
			double8 test_q1 = (double8) (1.0, 0.0, 0.0, 0.0, 1.0, 1.0, 0.0, 1.0);
			double8 test_q2 = (double8) (0.125, 0.0, 0.0, 0.0, 1.0, -1.0, 0.0, 2.0/3.0*(0.15-1.0));
			double8 test_u1 = q_to_u(test_q1);
			double8 test_u2 = q_to_u(test_q2);
			
			res_g[pid] = 0.0;
			res_g[pid] += test_u1.s0 - 1.0;
			res_g[pid] += test_u1.s1 - 0.0;
			res_g[pid] += test_u1.s2 - 0.0;
			res_g[pid] += test_u1.s3 - 0.0;
			res_g[pid] += test_u1.s4 - 1.0;
			res_g[pid] += test_u1.s5 - 1.0;
			res_g[pid] += test_u1.s6 - 0.0;
			res_g[pid] += test_u1.s7 - 2.5;

			res_g[pid] += test_u2.s0 - 0.125;
			res_g[pid] += test_u2.s1 - 0.0;
			res_g[pid] += test_u2.s2 - 0.0;
			res_g[pid] += test_u2.s3 - 0.0;
			res_g[pid] += test_u2.s4 - 1.0;
			res_g[pid] += test_u2.s5 + 1.0;
			res_g[pid] += test_u2.s6 - 0.0;
			res_g[pid] += test_u2.s7 - 0.15;
			return;
		}
		"""
		prg = cl.Program(ctx, source).build(ocl_build_option)
		a_np = np.random.rand(1).astype(np.float64)
		res_g = cl.Buffer(ctx, mf.WRITE_ONLY, a_np.nbytes)
		prg.test_q_to_u(queue, a_np.shape, None, res_g)
		res_np = np.empty_like(a_np)
		cl.enqueue_copy(queue, res_np, res_g)
		result = np.linalg.norm(res_np)
		self.assertAlmostEqual(result, 0.0)

	def test_get_eigenvalues_q(self):
		source = """
		#include "vector_util.h"
		__kernel void test_get_eigenvalues_q(__global double *res_g)
		{
			size_t pid = get_global_id(0);
			
			double8 test_q1 = (double8) (1.0, 0.0, 0.0, 0.0, 1.0, 1.0, 0.0, 1.0);
			//double8 test_q2 = (double8) (0.125, 0.0, 0.0, 0.0, 1.0, -1.0, 0.0, 2.0/3.0*(0.15-1.0));
			double8 test_eigen1 = get_eigenvalues_q(test_q1);
			//double8 test_eigen2 = get_eigenvalues_q(test_q2);
			
			res_g[pid] = 0.0;
			res_g[pid] += test_eigen1.s0 + 1.7706048719720355;
			res_g[pid] += test_eigen1.s1 + 1.0;
			res_g[pid] += test_eigen1.s2 + 0.72912622639400204;
			res_g[pid] += test_eigen1.s3 - 0.0;
			res_g[pid] += test_eigen1.s4 - 0.0;
			res_g[pid] += test_eigen1.s5 - 1.7706048719720355;
			res_g[pid] += test_eigen1.s6 - 1.0;
			res_g[pid] += test_eigen1.s7 - 0.72912622639400204;

			//res_g[pid] += test_eigen2.s0 - 0.125;
			//res_g[pid] += test_eigen2.s1 - 0.0;
			//res_g[pid] += test_eigen2.s2 - 0.0;
			//res_g[pid] += test_eigen2.s3 - 0.0;
			//res_g[pid] += test_eigen2.s4 - 1.0;
			//res_g[pid] += test_eigen2.s5 + 1.0;
			//res_g[pid] += test_eigen2.s6 - 0.0;
			//res_g[pid] += test_eigen2.s7 - 0.15;
			return;
		}
		"""
		prg = cl.Program(ctx, source).build(ocl_build_option)
		a_np = np.random.rand(1).astype(np.float64)
		res_g = cl.Buffer(ctx, mf.WRITE_ONLY, a_np.nbytes)
		prg.test_get_eigenvalues_q(queue, a_np.shape, None, res_g)
		res_np = np.empty_like(a_np)
		cl.enqueue_copy(queue, res_np, res_g)
		
		result = np.linalg.norm(res_np)
		self.assertAlmostEqual(result, 0.0)


	def test_get_eigenvalues_LR(self):
		source = """
		#include "vector_util.h"
		__kernel void test_get_eigenvalues_LR(__global double *res_g)
		{
			size_t pid = get_global_id(0);
			if(pid > 1) return;
			double8 test_q1 = (double8) (1.0, 0.0, 0.0, 0.0, 1.0, 1.0, 0.0, 1.0);
			
			double8 Lmatrix[8];
			double8 Rmatrix[8];
			get_eigenvectorLR(test_q1, Lmatrix, Rmatrix);
			
			for(size_t i = 0; i < 8 ;i++)
			{
				res_g[pid++] = Lmatrix[i].s0;
				res_g[pid++] = Lmatrix[i].s1;
				res_g[pid++] = Lmatrix[i].s2;
				res_g[pid++] = Lmatrix[i].s3;
				res_g[pid++] = Lmatrix[i].s4;
				res_g[pid++] = Lmatrix[i].s5;
				res_g[pid++] = Lmatrix[i].s6;
				res_g[pid++] = Lmatrix[i].s7;
			}
			for(size_t i = 0; i < 8 ;i++)
			{
				res_g[pid++] = Rmatrix[i].s0;
				res_g[pid++] = Rmatrix[i].s1;
				res_g[pid++] = Rmatrix[i].s2;
				res_g[pid++] = Rmatrix[i].s3;
				res_g[pid++] = Rmatrix[i].s4;
				res_g[pid++] = Rmatrix[i].s5;
				res_g[pid++] = Rmatrix[i].s6;
				res_g[pid++] = Rmatrix[i].s7;
			}
			return;
		}
		"""
		prg = cl.Program(ctx, source).build(ocl_build_option)
		a_np = np.random.rand(128).astype(np.float64)
		res_g = cl.Buffer(ctx, mf.WRITE_ONLY, a_np.nbytes)
		prg.test_get_eigenvalues_LR(queue, a_np.shape, None, res_g)
		res_np = np.empty_like(a_np)
		cl.enqueue_copy(queue, res_np, res_g)
		print('L:Minus Fast magneto sonic')
		print(res_np[0:8])
		print('L:Minus alfven sonic')
		print(res_np[8:16])
		print('L:Minus slow magneto sonic')
		print(res_np[16:24])
		print('L:entropy')
		print(res_np[24:32])

		print('L:divergence(magnetic flux wave)')
		print(res_np[32:40])
		self.assertAlmostEqual(np.linalg.norm(res_np[32:40]-[0.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0]),0.0)
		print('L:Plus slow magneto sonic')
		print(res_np[40:48])
		print('L:Plus alfven sonic')
		print(res_np[48:56])
		print('L:Plus Fast magneto sonic')
		print(res_np[56:64])

		print('R:Minus Fast magneto sonic')
		print(res_np[0+64:8+64])
		print('R:Minus alfven sonic')
		print(res_np[8+64:16+64])
		print('R:Minus slow magneto sonic')
		print(res_np[16+64:24+64])
		print('R:entropy')
		print(res_np[24+64:32+64])
		self.assertAlmostEqual(np.linalg.norm(res_np[24+64:32+64]-[1.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0]),0.0)
		print('R:divergence(magnetic flux wave)')
		print(res_np[32+64:40+64])
		self.assertAlmostEqual(np.linalg.norm(res_np[32+64:40+64]-[0.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0]),0.0)
		print('R:Plus slow magneto sonic')
		print(res_np[40+64:48+64])
		print('R:Plus alfven sonic')
		print(res_np[48+64:56+64])
		print('R:Plus Fast magneto sonic')
		print(res_np[56+64:64+64])


		
		
if __name__ == '__main__':
	unittest.main()
